﻿using System.Windows.Forms;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Xml;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using System.Linq;

namespace Cumuls
{
    public partial class Recup : Form
    {
        private string chemin_zip;                                                                                      // Chemin de l'archive .zip a extraire
        private string chemin_extract;                                                                                  // Chemin d'extraction de l'archive
        private string nom_archive;                                                                                     // Nom de l'archive .zip
        private string chemin_xml;                                                                                      // Chemin du fichier XML
        private DataTable dt = new DataTable();                                                                         // Table pour affichage dans Recup.cs
        private DataTable da = new DataTable();                                                                         // Table pour affichage des cumuls dans DetailsCumuls.cs
        private DataTable dp = new DataTable();                                                                         // Table des périodes
        private DetailsCumuls DC;                                                                                       // Variable pour DetailsCumuls.cs
        private DataSet data_set_cumuls = new DataSet();                                                                // Liste tables de cumuls
        private DataSet data_set_periodes = new DataSet();                                                              // Liste tables de périodes
        private int num_ligne;                                                                                          // Numéro de ligne, pour affichage des détails en fonction de la ligne sélectionnée

        public Recup()
        {
            InitializeComponent();
            this.Size = new Size(1000, 400);                                                                            // Taille de l'interface avant parsage du fichier xml
        }

        public bool CheckEquality(string v_key, string c_key)                                                           // Méthode pour comparaison de deux chaines de caractères pour le regroupment
        {

            if(v_key == c_key)                                                                                          // Si clé = valeur :
            {
                return true;                                                                                            // Retourne true
            }

            else                                                                                                        // Sinon :
            {
                return false;                                                                                           // Retourne false
            }
        }

        public Dictionary<Item, List<Item>> Commun(List<string> p_crit, List<string> p_choix, List<string> p_inu, int p_nb_erreurs, bool p_use_cumuls)                 // Méthode de création du dictionnaire
        {
            Dictionary<Item, List<Item>> dico_final = new Dictionary<Item, List<Item>>();                                                           // Crée un dictionnaire clé = item, valeur = List<Item>

            List<string> crit = p_crit;                                                                                 // Liste pour valeurs dans tableau des paramètres critiques
            List<string> choix = p_choix;                                                                               // Liste pour valeurs dans tableau des paramètres neutres
            List<string> inu = p_inu;                                                                                   // Liste pour valeurs dans tableau des paramètres inutiles
            int nb_erreurs  = p_nb_erreurs;
            bool use_cumuls = p_use_cumuls;

            List<Item> Items = new List<Item>();
            int nb_tables = data_set_cumuls.Tables.Count;                                                               // Récupère le nombre de tables dans data_set_cumuls (DataSet contenant les cumuls)

            DataTable table_base = (DataTable)dataGridView_rubrique.DataSource;                                         // Récupère la table utilisé dans l'afichage des cumuls

            int i = 0;
            foreach (DataTable table in data_set_cumuls.Tables)                                                         // Pour chaque table du dataset :
            {
                List<string> l_base = new List<string>();

                DataRow row_cumuls = table.Rows[0];                                                                     // Prend la première ligne de la table courante
                DataRow row_base = table_base.Rows[i];

                string codeFillingmethod = row_cumuls["CodeFillingMethod"].ToString();                                                // Récupere le code du cumul de la première ligne
                string type = row_cumuls["Type"].ToString();
                string sousType = row_cumuls["SousType"].ToString();
                string label = row_cumuls["Label"].ToString();
                string codeCumul = row_cumuls["CodeCumulLabel"].ToString();

                string natureUCode = row_base["Nature"].ToString();
                string subNatureUCode = row_base["SousNature"].ToString();

                string activityType = row_base["ActivityType"].ToString();
                string attachedperiod = row_base["AttachedPeriod"].ToString();
                string expenseAccount = row_base["ExpenseAccount"].ToString();
                string expenseState = row_base["ExpenseState"].ToString();
                string isBonus = row_base["IsBonus"].ToString();
                string isInWelcome = row_base["IsInWelcome"].ToString();
                string isPeriodic = row_base["IsPeriodic"].ToString();
                string organismeUCode = row_base["OrganismeUCode"].ToString();
                string paymentPeriodicity = row_base["PaymentPeriodicity"].ToString();
                string regimeUcode = row_base["RegimeUCode"].ToString();
                string uCodeAbsence = row_base["UCodeAbsence"].ToString();
                string uCodegroup = row_base["UCodeGroup"].ToString();

                string onlyInPilote = row_base["OnlyInPilote"].ToString();
                string advancedMode = row_base["AdvancedMode"].ToString();
                string advanceType = row_base["AdvanceType"].ToString();
                string canBeNotConcerned = row_base["CanBeNotConcerned"].ToString();
                string employerAmount = row_base["EmployerAmount"].ToString();
                string employerBasis = row_base["EmployerBasis"].ToString();
                string employerRate = row_base["EmployerRate"].ToString();
                string hasItemLinked = row_base["HasItemLinked"].ToString();
                string hasSickLeaveComplement = row_base["HasSickLeaveComplement"].ToString();
                string notInAccountingTransfert = row_base["NotInAccountingTransfert"].ToString();
                string themeId = row_base["ThemeId"].ToString();

                string contractId = row_base["ContractId"].ToString();
                string ccnParam = row_base["CCNParam"].ToString();
                string printCodeFlag = row_base["PrintCodeFlag"].ToString();
                string printCondition = row_base["PrintCondition"].ToString();

                if (codeCumul == "" || codeCumul == null)                                                                           // Si il n'y a pas de code, on passe a la table suivante
                {
                    continue;
                }

                else
                {
                    l_base.Add(label);                                                                                  // On ajoute le label de la table courante
                    l_base.Add(type);
                    l_base.Add(sousType);
                    l_base.Add(natureUCode);
                    l_base.Add(subNatureUCode);
                    l_base.Add(activityType);
                    l_base.Add(attachedperiod);
                    l_base.Add(expenseAccount);
                    l_base.Add(expenseState);
                    l_base.Add(isBonus);
                    l_base.Add(isInWelcome);
                    l_base.Add(isPeriodic);
                    l_base.Add(organismeUCode);
                    l_base.Add(paymentPeriodicity);
                    l_base.Add(regimeUcode);
                    l_base.Add(uCodeAbsence);
                    l_base.Add(uCodegroup);

                    l_base.Add(onlyInPilote);
                    l_base.Add(advancedMode);
                    l_base.Add(advanceType);
                    l_base.Add(canBeNotConcerned);
                    l_base.Add(employerAmount);
                    l_base.Add(employerBasis);
                    l_base.Add(employerRate);
                    l_base.Add(hasItemLinked);
                    l_base.Add(hasSickLeaveComplement);
                    l_base.Add(notInAccountingTransfert);
                    l_base.Add(themeId);

                    l_base.Add(contractId);
                    l_base.Add(ccnParam);
                    l_base.Add(printCodeFlag);
                    l_base.Add(printCondition);

                    foreach (DataRow filled_row in table.Rows)                                                          // Sinon, pour chaque ligne de la table parcourue
                    {
                        string code = filled_row["CodeCumulLabel"].ToString();                                          // On récupère le code de cumul et on l'ajoute a la liste
                        l_base.Add(code);                                                                               
                    }               

                    Item item = new Item(l_base);                                                                       // Création d'un item avec la liste de codes + label de la table
                    Items.Add(item);                                                                                    // Ajout de l'item a la liste d'items
                }

                i++; 
            }

            List<string> l_champs = new List<string>();

            l_champs.Add("Type");
            l_champs.Add("Sous Type");
            l_champs.Add("Nature");
            l_champs.Add("Sous Nature");
            l_champs.Add("ActivityType");
            l_champs.Add("AttachedPeriod");
            l_champs.Add("ExpenseAccount");
            l_champs.Add("ExpensesStateDoNotCheckRate");                                                        // Noms des propriétés a vérifier
            l_champs.Add("IsBonus");
            l_champs.Add("IsInWelcome");
            l_champs.Add("IsPeriodic");
            l_champs.Add("OrganismeUCode");
            l_champs.Add("PaymentPeriodicity");
            l_champs.Add("RegimeUCode");
            l_champs.Add("UCodeAbsence");
            l_champs.Add("UCodeGroup");

            l_champs.Add("OnlyInPilote");
            l_champs.Add("AdvancedMode");
            l_champs.Add("AdvanceType");
            l_champs.Add("CanBeNotConcerned");
            l_champs.Add("EmployerAmount");
            l_champs.Add("EmployerBasis");
            l_champs.Add("EmployerRate");
            l_champs.Add("HasItemLinked");
            l_champs.Add("HasSickLeaveComplement");
            l_champs.Add("NotInAccountingTransfert");
            l_champs.Add("ThemeId");

            l_champs.Add("ContractId");
            l_champs.Add("CCNParam");
            l_champs.Add("PrintCodeFlag");
            l_champs.Add("PrintCondition");

            foreach (Item item_courant in Items)                                                                        // Pour chaque items dans la liste
            {
                bool found = false;

                foreach (Item key in dico_final.Keys)                                                                   // Pour chaque clé du dictionnaire
                {
                    int nb_erreursFound = 0;

                    List<string> l_codes_key = new List<string>(key.GetCodes());                                        // Récupère la liste des valeurs composant la clé
                    List<string> l_codes_item_courant = new List<string>(item_courant.GetCodes());                      // Contient les codes de l'item courant

                    List<string> l_base_key = new List<string>();
                    List<string> l_courant = new List<string>();

                    

                    for (int index_key = 1; index_key < l_codes_key.Count; index_key++)
                    {
                        l_base_key.Add(l_codes_key[index_key]);                                                             // Valeurs de la première clé du dictionnaire
                    }

                    for (int index_courant = 1; index_courant < l_codes_item_courant.Count; index_courant++)
                    {
                        l_courant.Add(l_codes_item_courant[index_courant]);                                                 // Valeurs de l'item courant
                    }

                    for (int index = 0; index < 31; index++)
                    {
                        bool egal = CheckEquality(l_base_key[index], l_courant[index]);                                                 // Vérifie si les 2 champs sont égaux
                        string val = l_champs[index].ToString();

                        if (!egal)
                        {
                            if (p_crit.Contains(val))
                            {
                                nb_erreursFound = 9999999;
                                break;
                            }
                            else if (p_choix.Contains(val))
                            {
                                nb_erreursFound++;
                                if (nb_erreursFound > nb_erreurs)                                                                          // Si le nombre d'erreurs trouvées est égal au nombre saisi
                                    break;                                                                                                 // On passe a l'item suivant             
                            }
                            else if (p_inu.Contains(val))
                                continue;
                        }
                    }

                    if (use_cumuls)
                    {
                        for (int index_key = 31; index_key < l_base_key.Count; index_key++)
                        {
                            bool cumulFound = false;
                            for (int index_courant = 31; index_courant < l_courant.Count; index_courant++)
                            {
                                bool egal = CheckEquality(l_base_key[index_key], l_courant[index_courant]);                                       // Vérifie si les 2 champs sont égaux

                                if (egal)
                                {
                                    cumulFound = true;
                                    break;
                                }
                            }
                            if (!cumulFound)
                                nb_erreursFound++;
                        }
                    }

                    if (nb_erreursFound <= nb_erreurs)
                    {
                        dico_final[key].Add(item_courant);
                        found = true;
                        break;
                    }
                }

                if (!found)                                                                                             // Si l'item ne correspond pas a la clé :
                    dico_final.Add(item_courant, new List<Item>());                                                     // On crée une clé avec l'item courant
            }

            return dico_final;
        }

        private void button_upload_Click(object sender, System.EventArgs e)
        { 
            if (openFileDialog_uparchive.ShowDialog() == DialogResult.OK)
            {
                textBox_chemin_archive.Text = openFileDialog_uparchive.FileName;                                        // Chemin archive
                chemin_zip = openFileDialog_uparchive.FileName;
                nom_archive = openFileDialog_uparchive.SafeFileName;                                                    // Nom + extension
            }

            else
            {
                return;
            }

            if (!nom_archive.EndsWith(".zip"))
            {
                string messageError = "Impossible d'extraire, veuillez choisir une fichier .zip";
                string titreError = "Erreur";
                MessageBoxButtons boutonsError = MessageBoxButtons.OKCancel;
                MessageBoxIcon error = MessageBoxIcon.Error;
                DialogResult resError = MessageBox.Show(messageError, titreError, boutonsError, error);

                textBox_chemin_archive.Text = "";

                return;
            }

            Regex reg = new Regex(@"^[a-zA-Z0-9]{8}(-[a-zA-Z0-9]{4}){3}(-[a-zA-Z0-9]{8}){1}([0-9]{2}){1}[_.-a-zA-Z0-9]+9999.zip$");

            if (reg.IsMatch(nom_archive) == false){
                string messageError = "Nom d'archive non valide, veuillez réessayer";
                string titreError = "Erreur";
                MessageBoxButtons boutonsError = MessageBoxButtons.OKCancel;
                MessageBoxIcon error = MessageBoxIcon.Error;
                DialogResult resError = MessageBox.Show(messageError, titreError, boutonsError, error);

                textBox_chemin_archive.Text = "";

                return;
            }
        }

        private void button_extract_Click(object sender, System.EventArgs e)
        {
            if (folderBrowserDialog_upextract.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    chemin_extract = folderBrowserDialog_upextract.SelectedPath;
                    ZipFile.ExtractToDirectory(chemin_zip, chemin_extract);

                    string messageSucces = "Extraction réalisée avec succès dans " + chemin_extract;
                    string titre = "Succès";
                    MessageBoxButtons boutonsSucces = MessageBoxButtons.OK;
                    MessageBoxIcon succes = MessageBoxIcon.Information;
                    DialogResult resSucces = MessageBox.Show(messageSucces, titre, boutonsSucces, succes);
                }

                catch (System.ArgumentNullException)
                {
                    string messageError = "Veuillez choisir une archive à extraire";
                    string titreError = "Erreur";
                    MessageBoxButtons boutonsError = MessageBoxButtons.OKCancel;
                    MessageBoxIcon error = MessageBoxIcon.Error;
                    DialogResult resError = MessageBox.Show(messageError, titreError, boutonsError, error);
                }
            }

            else
            {
                string messageError = "Impossible d'extraire l'archive";
                string titreError = "Erreur";
                MessageBoxButtons boutonsError = MessageBoxButtons.OKCancel;
                MessageBoxIcon error = MessageBoxIcon.Error;
                DialogResult resError = MessageBox.Show(messageError, titreError, boutonsError, error);
            } 
        }

        private void button_fichier_Click(object sender, System.EventArgs e)
        {
            this.Size = new Size(1000, 400);
            dt.Clear();
            dataGridView_rubrique.Visible = false;

            label_category.Visible = false;
            label_des.Visible = false;
            label_type.Visible = false;
            label_lab.Visible = false;

            textBox_searchType.Text = string.Empty;
            textBox_searchCategory.Text = string.Empty;
            textBox_searchDes.Text = string.Empty;
            textBox_searchLabel.Text = string.Empty;

            if (openFileDialog_xml.ShowDialog() == DialogResult.OK)
            {
                chemin_xml = openFileDialog_xml.FileName;                                                                                                       // Chemin archive

                if (!chemin_xml.EndsWith("Item.xml"))
                {
                    string messageError = "Impossible de choisir ce fichier, veuillez choisir le fichier Item.xml ";
                    string titreError = "Erreur";
                    MessageBoxButtons boutonsError = MessageBoxButtons.OKCancel;
                    MessageBoxIcon error = MessageBoxIcon.Error;
                    DialogResult resError = MessageBox.Show(messageError, titreError, boutonsError, error);
                }

                else
                {
                    textBox_chemin_fichier.Text = chemin_xml;
                }
            }
        }

        private void button_parse_Click(object sender, System.EventArgs e)
        {
            try
            {
                Parser parser = new Parser(chemin_xml);
                List<Detail> rubriques = parser.Parse();

                List<string> l_dates = new List<string>();
                List<Cumul> l_cumuls = new List<Cumul>();

                dt = new DataTable();

                dt.Columns.Add("Type");
                dt.Columns.Add("SousType");
                dt.Columns.Add("Numéro");
                dt.Columns.Add("Catégorie");
                dt.Columns.Add("Description");
                dt.Columns.Add("Label");
                dt.Columns.Add("DisplayColumns");
                dt.Columns.Add("EmployeeAmount");
                dt.Columns.Add("EmployeeBasis");
                dt.Columns.Add("EmployeeRate");
                dt.Columns.Add("Nature");
                dt.Columns.Add("SousNature");

                dt.Columns.Add("ActivityType");
                dt.Columns.Add("AttachedPeriod");
                dt.Columns.Add("ExpenseAccount");
                dt.Columns.Add("ExpenseState");
                dt.Columns.Add("IsBonus");
                dt.Columns.Add("IsInWelcome");
                dt.Columns.Add("IsPeriodic");
                dt.Columns.Add("OrganismeUCode");
                dt.Columns.Add("PaymentPeriodicity");
                dt.Columns.Add("RegimeUCode");
                dt.Columns.Add("UCodeAbsence");
                dt.Columns.Add("UCodeGroup");

                dt.Columns.Add("OnlyInPilote");
                dt.Columns.Add("AdvancedMode");
                dt.Columns.Add("AdvanceType");
                dt.Columns.Add("CanBeNotConcerned");
                dt.Columns.Add("EmployerAmount");
                dt.Columns.Add("EmployerBasis");
                dt.Columns.Add("EmployerRate");
                dt.Columns.Add("HasItemLinked");
                dt.Columns.Add("HasSickLeaveComplement");
                dt.Columns.Add("NotInAccountingTransfert");
                dt.Columns.Add("ThemeId");

                dt.Columns.Add("ContractId");
                dt.Columns.Add("CCNParam");
                dt.Columns.Add("PrintCodeFlag");
                dt.Columns.Add("PrintCondition");

                dt.Columns.Add("Periodicity.January");
                dt.Columns.Add("Periodicity.February");
                dt.Columns.Add("Periodicity.March");
                dt.Columns.Add("Periodicity.April");
                dt.Columns.Add("Periodicity.May");
                dt.Columns.Add("Periodicity.June");
                dt.Columns.Add("Periodicity.July");
                dt.Columns.Add("Periodicity.August");
                dt.Columns.Add("Periodicity.September");
                dt.Columns.Add("Periodicity.October");
                dt.Columns.Add("Periodicity.November");
                dt.Columns.Add("Periodicity.December");

                dt.Columns.Add("Détails");
                dt.Columns.Add("LinkedItemLabel");

                string type;
                string sousType;
                string categorie;
                string description;
                string label;
                string columns;
                string employeeAmount;
                string employeeBasis;
                string employeeRate;
                string regimeUCodeLabel;
                string subNatureUCodeExportLabel;

                string activityType;
                string attachedPeriod;
                string expenseAccount;
                string expensesStateDoNotCheckRate;
                string isBonus;
                string isInWelcome;
                string isPeriodic;
                string organismeUCode;
                string paymentPeriodicity;
                string regimeUCode;
                string uCodeAbsence;
                string uCodeGroup;

                string onlyInPilote;
                string advancedMode;
                string advanceType;
                string canBeNotConcerned;
                string employerAmount;
                string employerBasis;
                string employerRate;
                string hasItemLinked;
                string hasSickLeaveComplement;
                string notInAccountingTransfert;
                string themeId;

                string contractId;
                string ccnParam;
                string printCodeFlag;
                string printCondition;

                string january;
                string february;
                string march;
                string april;
                string may;
                string june;
                string july;
                string august;
                string september;
                string october;
                string november;
                string december;

                string codeFillingMethod;
                string cumulType;
                string fillingMethod;
                string formula;
                string sign;
                string codeCumulLabel;
                string linkedItemLabel;

                num_ligne = 1;
                foreach (Detail une_rubrique in rubriques)
                {
                    da = new DataTable();                           // Table détails des cumuls
                    dp = new DataTable();                           // Table détails des périodes

                    da.Columns.Add("CodeFillingMethod");
                    da.Columns.Add("CumulType");
                    da.Columns.Add("FillingMethod");
                    da.Columns.Add("Formula");
                    da.Columns.Add("Sign");
                    da.Columns.Add("CodeCumulLabel");
                    da.Columns.Add("Type");
                    da.Columns.Add("SousType");
                    da.Columns.Add("Label");

                    dp.Columns.Add("January");
                    dp.Columns.Add("February");
                    dp.Columns.Add("March");
                    dp.Columns.Add("April");
                    dp.Columns.Add("May");
                    dp.Columns.Add("June");
                    dp.Columns.Add("July");
                    dp.Columns.Add("August");
                    dp.Columns.Add("September");
                    dp.Columns.Add("October");
                    dp.Columns.Add("November");
                    dp.Columns.Add("December");

                    type = une_rubrique.Type();
                    sousType = une_rubrique.SousType();
                    categorie = une_rubrique.ExportCategory();
                    description = une_rubrique.ExportDescription();
                    label = une_rubrique.ExportLabel();
                    columns = une_rubrique.DisplayColumn();
                    employeeAmount = une_rubrique.EmployeeAmount();
                    employeeBasis = une_rubrique.EmployeeBasis();
                    employeeRate = une_rubrique.EmployeeRate();

                    regimeUCodeLabel = une_rubrique.NatureCodeExportLabel();
                    subNatureUCodeExportLabel = une_rubrique.SubNatureUCodeExportLabel();

                    activityType = une_rubrique.ActivityType();
                    attachedPeriod = une_rubrique.AttachedPeriod();
                    expenseAccount = une_rubrique.ExpenseAccount();
                    expensesStateDoNotCheckRate = une_rubrique.ExpenseState();
                    isBonus = une_rubrique.IsBonus();
                    isInWelcome = une_rubrique.IsInWelcome();
                    isPeriodic = une_rubrique.IsPeriodic();
                    organismeUCode = une_rubrique.OrganismeUCode();
                    paymentPeriodicity = une_rubrique.PaymentPeriodicity();
                    regimeUCode = une_rubrique.RegimeUCode();
                    uCodeAbsence = une_rubrique.UCodeAbsence();
                    uCodeGroup = une_rubrique.UCodeGroup();

                    onlyInPilote = une_rubrique.OnlyInPilote();
                    advancedMode = une_rubrique.AdvancedMode();
                    advanceType = une_rubrique.AdvanceType();
                    canBeNotConcerned = une_rubrique.CanBeNotConcerned();
                    employerAmount = une_rubrique.EmployerAmount();
                    employerBasis = une_rubrique.EmployerBasis();
                    employerRate = une_rubrique.EmployerRate();
                    hasItemLinked = une_rubrique.HasItemLinked();
                    hasSickLeaveComplement = une_rubrique.HasSickLeaveComplement();
                    notInAccountingTransfert = une_rubrique.NotInAccountingTransfert();
                    themeId = une_rubrique.ThemeId();

                    contractId = une_rubrique.ContractId();
                    ccnParam = une_rubrique.CcnParam();
                    printCodeFlag = une_rubrique.PrintCodeFlag();
                    printCondition = une_rubrique.PrintCondition();

                    linkedItemLabel = une_rubrique.LinkedItemLabel();

                    l_dates = une_rubrique.Dates();
                    l_cumuls = une_rubrique.Cumuls();

                    january = l_dates[0];
                    february = l_dates[1];
                    march = l_dates[2];
                    april = l_dates[3];
                    may = l_dates[4];
                    june = l_dates[5];
                    july = l_dates[6];
                    august = l_dates[7];
                    september = l_dates[8];
                    october = l_dates[9];
                    november = l_dates[10];
                    december = l_dates[11];

                    dp.Rows.Add(new object[] { january, february, march, april, may, june, july, august, september, october, november, december });

                    string details = "Voir";
                    dt.Rows.Add(new object[] { type, sousType, num_ligne, categorie, description, label, columns, employeeAmount, employeeBasis, employeeRate, regimeUCodeLabel, subNatureUCodeExportLabel,
                        activityType, attachedPeriod, expenseAccount, expensesStateDoNotCheckRate, isBonus, isInWelcome, isPeriodic, organismeUCode, paymentPeriodicity, regimeUCode, uCodeAbsence, uCodeGroup,
                         onlyInPilote, advancedMode, advanceType, canBeNotConcerned, employerAmount, employerBasis, employerRate, hasItemLinked, hasSickLeaveComplement, notInAccountingTransfert, themeId,
                         contractId, ccnParam, printCodeFlag, printCondition, january, february, march, april, may, june, july, august, september, october, november, december, details, linkedItemLabel });

                    foreach (Cumul cumul in l_cumuls)
                    {
                        codeFillingMethod = cumul.CodeFillingMethod();
                        cumulType = cumul.CumulType();
                        fillingMethod = cumul.FillingMethod();
                        formula = cumul.Formula();
                        sign = cumul.Sign();
                        codeCumulLabel = cumul.CodeCumulLabel();

                        da.Rows.Add(codeFillingMethod, cumulType, fillingMethod, formula, sign, codeCumulLabel, type, sousType, label);
                    }

                    data_set_cumuls.Tables.Add(da);
                    data_set_periodes.Tables.Add(dp);
                    num_ligne++;
                }

                this.Size = new Size(1620, 950);

                dataGridView_rubrique.DataSource = dt;
                dataGridView_rubrique.Visible = true;

                int index_base = dataGridView_rubrique.Columns["SousNature"].Index;
                int index_details = dataGridView_rubrique.Columns["Détails"].Index;
            
                dataGridView_rubrique.Columns[0].Visible = false;       // Cache le numéro de Type de rubriques
                dataGridView_rubrique.Columns[1].Visible = false;       // Cache le numéro de sous type
                dataGridView_rubrique.Columns[2].Visible = false;       // Cache le numéro de la ligne

                for (int i = index_base + 1; i < index_details; i++)
                {
                    dataGridView_rubrique.Columns[i].Visible = false;           // Cache les périodes
                }

                label_type.Visible = true;
                label_category.Visible = true;
                label_des.Visible = true;
                label_lab.Visible = true;
                textBox_searchType.Visible = true;
                textBox_searchCategory.Visible = true;
                textBox_searchDes.Visible = true;
                textBox_searchLabel.Visible = true;
                button_critères.Visible = true;
                button_clear.Visible = true;
            }

            catch (System.Exception ex)
            {
                if (ex is System.ArgumentNullException)
                {
                    string messageError = "Veuillez choisir un fichier avant de parser";
                    string titreError = "Erreur";
                    MessageBoxButtons boutonsError = MessageBoxButtons.OKCancel;
                    MessageBoxIcon error = MessageBoxIcon.Error;
                    DialogResult resError = MessageBox.Show(messageError, titreError, boutonsError, error);
                    return;
                }

                else if (ex is System.Data.DuplicateNameException)
                {
                    string messageError = "Veuillez choisir un autre fichier a parser";
                    string titreError = "Erreur";
                    MessageBoxButtons boutonsError = MessageBoxButtons.OKCancel;
                    MessageBoxIcon error = MessageBoxIcon.Error;
                    DialogResult resError = MessageBox.Show(messageError, titreError, boutonsError, error);
                    return;
                }
            }
        }

        private void dataGridView_rubrique_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow current = dataGridView_rubrique.CurrentCell.OwningRow;
            string value = current.Cells["Numéro"].Value.ToString();
            int ligne;
            ligne = int.Parse(value);
            ligne -= 1;

            if(Application.OpenForms.OfType<DetailsCumuls>().Any())
            {
                Application.OpenForms.OfType<DetailsCumuls>().First().Close();                                                              // Bloque l'ouvertures de plusieurs cumuls a la fois
            }

            DataTable recup_cumuls = data_set_cumuls.Tables[ligne];
            DataTable recup_periodes = data_set_periodes.Tables[ligne];

            DataRow row = recup_cumuls.Rows[0];

            string type = row["Type"].ToString();
            string sousType = row["SousType"].ToString();

            DC = new DetailsCumuls(recup_cumuls, recup_periodes, type, sousType);            
            DC.Show();
        }

        private void textBox_searchType_TextChanged(object sender, System.EventArgs e)
        {
            DataView dv = dt.DefaultView;

            if (textBox_searchCategory.Text == "" && textBox_searchLabel.Text == "" && textBox_searchDes.Text == "")
            {
                dv.RowFilter = string.Format("Type like '%{0}%'", textBox_searchType.Text);
                dataGridView_rubrique.DataSource = dv;
            }

            else if (textBox_searchLabel.Text != "" || textBox_searchType.Text != null)
            {
                dv.RowFilter = string.Format("Type like '%{0}%' and Label like '%{1}%'", textBox_searchType.Text, textBox_searchLabel.Text);
                dataGridView_rubrique.DataSource = dv;
            }

            else if (textBox_searchDes.Text != "" || textBox_searchType.Text != null)
            {
                dv.RowFilter = string.Format("Type like '%{0}%' and Description like '%{1}%'", textBox_searchType.Text, textBox_searchDes.Text);
                dataGridView_rubrique.DataSource = dv;
            }

            else if (textBox_searchLabel.Text != "" || textBox_searchCategory.Text != null)
            {
                dv.RowFilter = string.Format("Type like '%{0}%' and Label like '%{1}%'", textBox_searchType.Text, textBox_searchLabel.Text);
                dataGridView_rubrique.DataSource = dv;
            }

            else
            {
                dv.RowFilter = string.Format("Type like '%{0}%' and Catégorie like '%{1}%' and Description like '%{2}%' and Label like '%{3}%'", textBox_searchType.Text, textBox_searchCategory.Text, textBox_searchDes.Text, textBox_searchLabel.Text);
                dataGridView_rubrique.DataSource = dv;
            }
        }

        private void textBox_searchCategory_TextChanged(object sender, System.EventArgs e)
        {
            DataView dv = dt.DefaultView;

            if (textBox_searchType.Text == "" && textBox_searchLabel.Text == "" && textBox_searchDes.Text == "")
            {
                dv.RowFilter = string.Format("Catégorie like '%{0}%'", textBox_searchCategory.Text);
                dataGridView_rubrique.DataSource = dv;
            }

            else if (textBox_searchLabel.Text != "" && textBox_searchCategory.Text != null)
            {
                dv.RowFilter = string.Format("Catégorie like '%{0}%' and Label like '%{1}%'", textBox_searchCategory.Text, textBox_searchLabel.Text);
                dataGridView_rubrique.DataSource = dv;
            }

            else if (textBox_searchDes.Text != "" && textBox_searchCategory.Text != null)
            {
                dv.RowFilter = string.Format("Catégorie like '%{0}%' and Description like '%{1}%'", textBox_searchCategory.Text, textBox_searchDes.Text);
                dataGridView_rubrique.DataSource = dv;
            }

            else if (textBox_searchType.Text != "" && textBox_searchCategory.Text != null)
            {
                dv.RowFilter = string.Format("Catégorie like '%{0}%' and Type like '%{1}%'", textBox_searchCategory.Text, textBox_searchType.Text);
                dataGridView_rubrique.DataSource = dv;
            }

            else
            {
                dv.RowFilter = string.Format("Catégorie like '%{0}%' and Type like '%{1}%' and Description like '%{2}%' and Label like '%{3}%'", textBox_searchCategory.Text, textBox_searchType.Text, textBox_searchDes.Text, textBox_searchType.Text);
                dataGridView_rubrique.DataSource = dv;
            }
        }

        private void textBox_searchDes_TextChanged(object sender, System.EventArgs e)
        {
            DataView dv = dt.DefaultView;

            if (textBox_searchType.Text == "" && textBox_searchLabel.Text == "" && textBox_searchLabel.Text == "")
            {
                dv.RowFilter = string.Format("Description like '%{0}%'", textBox_searchDes.Text);
                dataGridView_rubrique.DataSource = dv;
            }

            else if (textBox_searchDes.Text != "" && textBox_searchCategory.Text != null)
            {
                dv.RowFilter = string.Format("Description like '%{0}%' and Catégorie like '%{1}%'", textBox_searchDes.Text, textBox_searchCategory.Text);
                dataGridView_rubrique.DataSource = dv;
            }

            else if (textBox_searchDes.Text != "" && textBox_searchLabel.Text != null)
            {
                dv.RowFilter = string.Format("Description like '%{0}%' and Label like '%{1}%'", textBox_searchDes.Text, textBox_searchLabel.Text);
                dataGridView_rubrique.DataSource = dv;
            }

            else if (textBox_searchDes.Text != "" && textBox_searchType.Text != null)
            {
                dv.RowFilter = string.Format("Description like '%{0}%' and Type like '%{1}%'", textBox_searchDes.Text, textBox_searchType.Text);
                dataGridView_rubrique.DataSource = dv;
            }

            else
            {
                dv.RowFilter = string.Format("Description like '%{0}%' and Type like '%{1}%' and Catégorie like '%{2}%' and Label like '%{3}%'", textBox_searchDes.Text, textBox_searchType.Text, textBox_searchCategory.Text, textBox_searchLabel.Text);
                dataGridView_rubrique.DataSource = dv;
            }
        }

        private void textBox_searchLabel_TextChanged(object sender, System.EventArgs e)
        {
            DataView dv = dt.DefaultView;

            if (textBox_searchType.Text == "" && textBox_searchCategory.Text == "" && textBox_searchDes.Text == ""){
                dv.RowFilter = string.Format("Label like '%{0}%'", textBox_searchLabel.Text);
                dataGridView_rubrique.DataSource = dv;
            }

            else if(textBox_searchLabel.Text != "" && textBox_searchCategory.Text != null)
            {
                dv.RowFilter = string.Format("Label like '%{0}%' and Catégorie like '%{1}%'", textBox_searchLabel.Text, textBox_searchCategory.Text);
                dataGridView_rubrique.DataSource = dv;
            }

            else if (textBox_searchDes.Text != "" && textBox_searchLabel.Text != null)
            {
                dv.RowFilter = string.Format("Label like '%{0}%' and Description like '%{1}%'", textBox_searchLabel.Text, textBox_searchDes.Text);
                dataGridView_rubrique.DataSource = dv;
            }

            else if (textBox_searchLabel.Text != "" && textBox_searchType.Text != null)
            {
                dv.RowFilter = string.Format("Label like '%{0}%' and Type like '%{1}%'", textBox_searchLabel.Text, textBox_searchType.Text);
                dataGridView_rubrique.DataSource = dv;
            }

            else
            {
                dv.RowFilter = string.Format("Label like '%{0}%' and Type like '%{1}%' and Catégorie like '%{2}%' and Description like '%{3}%'", textBox_searchLabel.Text, textBox_searchType.Text, textBox_searchCategory.Text, textBox_searchDes.Text);
                dataGridView_rubrique.DataSource = dv;
            }
        }

        private void button_clear_Click(object sender, System.EventArgs e)
        {
            textBox_searchType.Text = "";
            textBox_searchCategory.Text = "";
            textBox_searchDes.Text = "";
            textBox_searchLabel.Text = "";

            var ds = dataGridView_rubrique.DataSource;
            dataGridView_rubrique.DataSource = null;
            dataGridView_rubrique.DataSource = ds;

            int index_base = dataGridView_rubrique.Columns["SousNature"].Index;
            int index_details = dataGridView_rubrique.Columns["Détails"].Index;

            dataGridView_rubrique.Columns[0].Visible = false;                                                               // Cache le numéro de Type de rubriques
            dataGridView_rubrique.Columns[1].Visible = false;                                                               // Cache le numéro de la ligne

            for (int i = index_base + 1; i < index_details; i++)
            {
                dataGridView_rubrique.Columns[i].Visible = false;                                                           // Cache les périodes
            }
        }

        private void button_communs_Click(object sender, System.EventArgs e)
        {
            if (Application.OpenForms.OfType<Criteres>().Any())                                                             // Si Critères.cs est déjà ouvert :
            {
                Application.OpenForms.OfType<Criteres>().First().Close();                                                   // On ferme la fenetre
            }

            int nb_rubriques = data_set_cumuls.Tables.Count;

            Criteres crit = new Criteres(this, nb_rubriques);                                                                             // Crée une instance de Critères.cs avec cette instance de recup.cs en paramètre
            crit.Show();                                                                                                    // Affiche l'instance de Critères
        }
    }
}
