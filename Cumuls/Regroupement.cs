﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cumuls
{
    public partial class Regroupement : Form
    {
        private Dictionary<Item, List<Item>> dico_commun = new Dictionary<Item, List<Item>>();
        private DataTable dt_groupe = new DataTable();
        private DataTable dt_details = new DataTable();
        private DataSet data_set_communs = new DataSet();
        int num_ligne;

        public Regroupement(Dictionary<Item, List<Item>> p_dico_commun, int p_nb_rubriques)
        {
            InitializeComponent();
            dico_commun = p_dico_commun;
            this.Size = new Size(620, 680);
            int nb_rubriques = p_nb_rubriques;
            int nb_rubriques_groupe = dico_commun.Count;
            string message = nb_rubriques + " rubriques ont été trouvées";
            string message_groupe = nb_rubriques_groupe + " groupement de rubriques ont été faits";
            label_nb_rubrique.Text = message;
            label_nb_rubrique_groupe.Text = message_groupe;

            dt_groupe = new DataTable();
            dt_groupe.Columns.Add("Numéro");
            dt_groupe.Columns.Add("Type");
            dt_groupe.Columns.Add("Nature");
            dt_groupe.Columns.Add("Groupe");
            dt_groupe.Columns.Add("Rubriques");

            string k_type;
            string k_sousType;
            string k_nature;
            string k_label;

            string k_sousNature;
            string k_activityType;
            string k_attachedPeriod;
            string k_expenseAccount;
            string k_expenseState;
            string k_isBonus;
            string k_isInWelcome;
            string k_isPeriodic;

            string v_sousType;
            string v_nature;
            string v_label;

            string v_sousNature;
            string v_activityType;
            string v_attachedPeriod;
            string v_expenseAccount;
            string v_expenseState;
            string v_isBonus;
            string v_isInWelcome;
            string v_isPeriodic;

  
            List<string> l_key = new List<string>();
            List<string> l_value = new List<string>();
            num_ligne = 1;

            foreach(Item key in dico_commun.Keys)
            {
                int nb_row = dico_commun[key].Count;

                l_key = key.GetCodes();
                k_label = l_key[0];
                k_type = l_key[1];
                k_sousType = l_key[2];
                k_nature = l_key[3];

                k_sousNature = l_key[4];
                k_activityType = l_key[5];
                k_attachedPeriod = l_key[6];
                k_expenseAccount = l_key[7];
                k_expenseState = l_key[8];
                k_isBonus = l_key[9];
                k_isInWelcome = l_key[10];
                k_isPeriodic = l_key[11];

                dt_details = new DataTable();
                dt_details.Columns.Add("Sous Type");
                dt_details.Columns.Add("Nature");

                dt_details.Columns.Add("Sous Nature");
                dt_details.Columns.Add("ActivityType");
                dt_details.Columns.Add("AttachedPeriod");
                dt_details.Columns.Add("ExpenseAccount");
                dt_details.Columns.Add("ExpensesStateDoNotCheckRate");
                dt_details.Columns.Add("IsBonus");
                dt_details.Columns.Add("IsInWelcome");
                dt_details.Columns.Add("IsPeriodic");

                dt_details.Columns.Add("Label");
                string grp = "Groupe " + num_ligne;

                dt_groupe.Rows.Add(new object[] { num_ligne, k_type, k_nature, grp, nb_row + 1 });
                dt_details.Rows.Add(new object[] { k_sousType, k_nature, k_sousNature, k_activityType, k_attachedPeriod, k_expenseAccount, k_expenseState, k_isBonus, k_isInWelcome, k_isPeriodic, k_label });

                foreach (Item value in dico_commun[key])
                {
                    l_value = value.GetCodes();
                    v_label = l_value[0];
                    v_sousType = l_value[2];
                    v_nature = l_value[3];

                    v_sousNature = l_value[4];
                    v_activityType = l_value[5];
                    v_attachedPeriod = l_value[6];
                    v_expenseAccount = l_value[7];
                    v_expenseState = l_value[8];
                    v_isBonus = l_value[9];
                    v_isInWelcome = l_value[10];
                    v_isPeriodic = l_value[11];

                    dt_details.Rows.Add(new object[] { v_sousType, v_nature, v_sousNature, v_activityType, v_attachedPeriod, v_expenseAccount, v_expenseState, v_isBonus, v_isInWelcome, v_isPeriodic, v_label });
                }

                data_set_communs.Tables.Add(dt_details);
                num_ligne++;
            }

            dataGridView_groupes.DataSource = dt_groupe;
        }

        private void dataGridView_groupes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            this.Size = new Size(1650,680);
            DataGridViewRow current = dataGridView_groupes.CurrentCell.OwningRow;
            string value = current.Cells["Numéro"].Value.ToString();
            int ligne;
            ligne = int.Parse(value);
            ligne -= 1;

            DataTable recup_commun = data_set_communs.Tables[ligne];

            if(dataGridView_communs.Visible == true)
            {
                dataGridView_communs.Visible = false;
                dataGridView_communs.DataSource = recup_commun;
                dataGridView_communs.Visible = true;
            }

            else
            {
                dataGridView_communs.DataSource = recup_commun;
                dataGridView_communs.Visible = true;
            }
        }
    }
}
