﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;
using System.Data;
using System.Windows.Forms;
using System.IO;

namespace Cumuls
{
    class Parser
    {
        private string GetAttribute(XmlNode node, string attribute)                                                                     // Méthode prennant en paramètre un noeud XML ainsi qu'un attribut a trouver
        {
            try
            {
                if (node.Attributes == null || node.Attributes.Count == 0)                                                              // Si le noeud n'a pas d'attributs
                {
                    return null;                                                                                                        // Retourne null
                }                   

                XmlAttribute attrib = node.Attributes[attribute];                                                                       // Si la valeur de l'attribut est nulle
                if (attrib == null)
                {
                    return null;                                                                                                        // Retourne null
                }                

                return attrib.InnerText;                                                                                                // Sinon, retourne la valeur de l'attribut
            }

            catch (Exception)
            {
                return null;                                                                                                            // Retourne null si le bloc Try échoue
            }   
        }

        private String path;                                                                                                            // Chemin du fichier a parser
        private XmlDocument doc;

        public Parser(String p_Path)
        {
            path = p_Path;
        }

        public List<Detail> Parse()
        {
            List<Detail> l_rubriques = new List<Detail>();                                                                              // Crée une liste de rubriques

            doc = new XmlDocument();
            doc.Load(path);
            XmlNode root = doc.DocumentElement;                                                                                         // Récupère la racine du fichier XML                                                                                 

            XmlNodeList l_type = doc.SelectNodes("EBP/Content/HistorizedItemEntity/Type");                                              // --------------------------------------------------------------
            XmlNodeList l_sousType = doc.SelectNodes("EBP/Content/HistorizedItemEntity/SubType");                                       //
            XmlNodeList l_exportCode = doc.SelectNodes("EBP/Content/HistorizedItemEntity");                                             //
            XmlNodeList l_exportDescription = doc.SelectNodes("EBP/Content/HistorizedItemEntity");                                      //
            XmlNodeList l_exportLabel = doc.SelectNodes("EBP/Content/HistorizedItemEntity");                                            //
            XmlNodeList l_displayColumn = doc.SelectNodes("EBP/Content/HistorizedItemEntity/DisplayColumn");                            // Récupere tout les noeuds du fichier et stock dans une liste
            XmlNodeList l_employeeAmount = doc.SelectNodes("EBP/Content/HistorizedItemEntity/EmployeeAmount");                          //
            XmlNodeList l_employeeBasis = doc.SelectNodes("EBP/Content/HistorizedItemEntity/EmployeeBasis");                            //
            XmlNodeList l_employeeRate = doc.SelectNodes("EBP/Content/HistorizedItemEntity/EmployeeRate");                              //
            XmlNodeList l_natureUCodeExportLabel = doc.SelectNodes("EBP/Content/HistorizedItemEntity/NatureUCode");                     //
            XmlNodeList l_subNatureUCodeExportLabel = doc.SelectNodes("EBP/Content/HistorizedItemEntity/SubNatureUCode");               // ---------------------------------------------------------------

            XmlNodeList l_nodesCumuls = doc.GetElementsByTagName("HistorizedItemEntity");                                               // Récupere les listes de cumuls de chaque entité et stock dans la liste

            XmlNodeList l_january = doc.SelectNodes("EBP/Content/HistorizedItemEntity/Periodicity.January");                            // ---------------------------------------------------------------
            XmlNodeList l_february = doc.SelectNodes("EBP/Content/HistorizedItemEntity/Periodicity.February");                          //
            XmlNodeList l_march = doc.SelectNodes("EBP/Content/HistorizedItemEntity/Periodicity.March");                                //
            XmlNodeList l_april = doc.SelectNodes("EBP/Content/HistorizedItemEntity/Periodicity.April");                                //
            XmlNodeList l_may = doc.SelectNodes("EBP/Content/HistorizedItemEntity/Periodicity.May");                                    //
            XmlNodeList l_june = doc.SelectNodes("EBP/Content/HistorizedItemEntity/Periodicity.June");                                  // Récupère tous les noeuds des périodes du fichier
            XmlNodeList l_july = doc.SelectNodes("EBP/Content/HistorizedItemEntity/Periodicity.July");                                  //
            XmlNodeList l_august = doc.SelectNodes("EBP/Content/HistorizedItemEntity/Periodicity.August");                              //
            XmlNodeList l_september = doc.SelectNodes("EBP/Content/HistorizedItemEntity/Periodicity.September");                        //
            XmlNodeList l_october = doc.SelectNodes("EBP/Content/HistorizedItemEntity/Periodicity.October");                            //
            XmlNodeList l_november = doc.SelectNodes("EBP/Content/HistorizedItemEntity/Periodicity.November");                          //
            XmlNodeList l_december = doc.SelectNodes("EBP/Content/HistorizedItemEntity/Periodicity.December");                          // ---------------------------------------------------------------

            XmlNodeList l_activityType = doc.SelectNodes("EBP/Content/HistorizedItemEntity/ActivityType");                              // ---------------------------------------------------------------
            XmlNodeList l_attachedPeriod = doc.SelectNodes("EBP/Content/HistorizedItemEntity/AttachedPeriod");                          //
            XmlNodeList l_expenseAccount = doc.SelectNodes("EBP/Content/HistorizedItemEntity/ExpenseAccount");                          //
            XmlNodeList l_expensesState = doc.SelectNodes("EBP/Content/HistorizedItemEntity/ExpensesStateDoNotCheckRate");              //
            XmlNodeList l_isBonus = doc.SelectNodes("EBP/Content/HistorizedItemEntity/IsBonus");                                        //
            XmlNodeList l_isInWelcome = doc.SelectNodes("EBP/Content/HistorizedItemEntity/IsInWelcome");                                //
            XmlNodeList l_isPeriodic = doc.SelectNodes("EBP/Content/HistorizedItemEntity/IsPeriodic");                                  //
            XmlNodeList l_organismeUCode = doc.SelectNodes("EBP/Content/HistorizedItemEntity/OrganismeUCode");                          //
            XmlNodeList l_paymentPeriodicity = doc.SelectNodes("EBP/Content/HistorizedItemEntity/PaymentPeriodicity");                  //
            XmlNodeList l_regimeUCode = doc.SelectNodes("EBP/Content/HistorizedItemEntity/RegimeUCode");                                //
            XmlNodeList l_uCodeAbsence = doc.SelectNodes("EBP/Content/HistorizedItemEntity/UCodeAbsence");                              //
            XmlNodeList l_uCodeGroup = doc.SelectNodes("EBP/Content/HistorizedItemEntity/UCodeGroup");                                  //
            XmlNodeList l_onlyInPilote = doc.SelectNodes("EBP/Content/HistorizedItemEntity/OnlyInPilote");                              //
            XmlNodeList l_advancedMode = doc.SelectNodes("EBP/Content/HistorizedItemEntity/AdvancedMode");                              // Récupere les noeuds du fichier et stock dans une liste
            XmlNodeList l_advanceType = doc.SelectNodes("EBP/Content/HistorizedItemEntity/AdvanceType");                                // 
            XmlNodeList l_canBeNotConcerned = doc.SelectNodes("EBP/Content/HistorizedItemEntity/CanBeNotConcerned");                    //
            XmlNodeList l_employerAmount = doc.SelectNodes("EBP/Content/HistorizedItemEntity/EmployerAmount");                          //
            XmlNodeList l_employerBasis = doc.SelectNodes("EBP/Content/HistorizedItemEntity/EmployerBasis");                            //
            XmlNodeList l_employerRate = doc.SelectNodes("EBP/Content/HistorizedItemEntity/EmployerRate");                              //
            XmlNodeList l_hasItemLinked = doc.SelectNodes("EBP/Content/HistorizedItemEntity/HasItemLinked");                            //
            XmlNodeList l_hasSickLeaveComplement = doc.SelectNodes("EBP/Content/HistorizedItemEntity/HasSickLeaveComplement");          //
            XmlNodeList l_notInAccountingTransfert = doc.SelectNodes("EBP/Content/HistorizedItemEntity/NotInAccountingTransfert");      //
            XmlNodeList l_themeId = doc.SelectNodes("EBP/Content/HistorizedItemEntity/ThemeId");                                        //
            XmlNodeList l_contractId = doc.SelectNodes("EBP/Content/HistorizedItemEntity/ContractId");                                  //
            XmlNodeList l_ccnParam = doc.SelectNodes("EBP/Content/HistorizedItemEntity/CcnParam");                                      //
            XmlNodeList l_printCodeFlag = doc.SelectNodes("EBP/Content/HistorizedItemEntity/PrintCodeFlag");                            //
            XmlNodeList l_printCondition = doc.SelectNodes("EBP/Content/HistorizedItemEntity/PrintCondition");                          // --------------------------------------------------------------

            string type;
            string sousType;
            string exportCode;
            string exportDescription; ;
            string exportLabel;
            string displayColumn;
            string employeeAmount;
            string employeeBasis;
            string employeeRate;
            string natureUCodeExportLabel;
            string subNatureUCodeExportLabel;

            string activityType;
            string attachedPeriod;
            string expenseAccount;
            string expensesStateDoNotCheckRate;
            string isBonus;
            string isInWelcome;
            string isPeriodic;
            string organismeUCode;
            string paymentPeriodicity;
            string regimeUCode;
            string uCodeAbsence;
            string uCodeGroup;

            string onlyInPilote;
            string advancedMode;
            string advanceType;
            string canBeNotConcerned;
            string employerAmount;
            string employerBasis;
            string employerRate;
            string hasItemLinked;
            string hasSickLeaveComplement;
            string notInAccountingTransfert;
            string themeId;

            string codeFillingMethod = "";                                                                                              //
            string cumulType = "";                                                                                                      //
            string fillingMethod = "";                                                                                                  //
            string formula = "";                                                                                                        // Initialise les valeurs des cumuls à vide, les cumuls n'étant pas toujours
            string sign = "";                                                                                                           // présents dans la rubrique parcourue
            string uCodeCumul = "";                                                                                                     //
            string linkedItemLabel = "";                                                                                                //

            string january;
            string february;
            string march;
            string april;
            string may;
            string june;
            string july;
            string august;
            string september;
            string october;
            string november;
            string december;

            string contractId;
            string ccnParam;
            string printCodeFlag;
            string printCondition;

            for (int i = 0; i < l_type.Count; i++)                                                                                      // Pour chaque noeud dans l_type ( Nombre de rubriques dans le fichier )
            {
                XmlNode n_type = l_type[i];
                XmlNode n_sousType = l_sousType[i];
                XmlNode n_exportCode = l_exportCode[i];
                XmlNode n_exportDescription = l_exportDescription[i];
                XmlNode n_exportLabel = l_exportLabel[i];
                XmlNode n_displayColumn = l_displayColumn[i];
                XmlNode n_employeeAmount = l_employeeAmount[i];
                XmlNode n_employeebasis = l_employeeBasis[i];
                XmlNode n_employeeRate = l_employeeRate[i];
                XmlNode n_natureUCode = l_natureUCodeExportLabel[i];
                XmlNode n_subNature = l_subNatureUCodeExportLabel[i];

                XmlNode n_activityType = l_activityType[i];
                XmlNode n_attachedperiod = l_attachedPeriod[i];
                XmlNode n_expenseAccount = l_expenseAccount[i];
                XmlNode n_expenseState = l_expensesState[i];
                XmlNode n_isBonus = l_isBonus[i];
                XmlNode n_isInWelcome = l_isInWelcome[i];
                XmlNode n_isPeriodic = l_isPeriodic[i];
                XmlNode n_organismeUCode = l_organismeUCode[i];
                XmlNode n_paymentPeriodicity = l_paymentPeriodicity[i];                                                                 // Crée un noeud a partir d'une liste et d'un index
                XmlNode n_regimeUCode = l_regimeUCode[i];
                XmlNode n_uCodeAbsence = l_uCodeAbsence[i];
                XmlNode n_uCodeGroup = l_uCodeGroup[i];

                XmlNode n_onlyInPilote = l_onlyInPilote[i];
                XmlNode n_advancedMode = l_advancedMode[i];
                XmlNode n_advanceType = l_advanceType[i];
                XmlNode n_canBeNotConcerned = l_canBeNotConcerned[i];
                XmlNode n_employerAmount = l_employerAmount[i];
                XmlNode n_employerBasis = l_employerBasis[i];
                XmlNode n_employerRate = l_employerRate[i];
                XmlNode n_hasItemLinked = l_hasItemLinked[i];
                XmlNode n_hasSickLeaveComplement = l_hasSickLeaveComplement[i];
                XmlNode n_notInAccountingTransfert = l_notInAccountingTransfert[i];
                XmlNode n_themeId = l_themeId[i];

                XmlNode n_contractId = l_contractId[i];
                XmlNode n_ccnParam = l_ccnParam[i];
                XmlNode n_printCodeFlag = l_printCodeFlag[i];
                XmlNode n_printCondition = l_printCondition[i];

                List<Cumul> cumuls = new List<Cumul>();                                                                                 // Crée une liste de cumuls

                XmlNode nodeCurrent = l_nodesCumuls[i];                                                                                 // Récupere le noeud courant pouvant contenir plusieurs cumuls

                XmlNodeList l_uCodeCumul = nodeCurrent.SelectNodes("ItemCumuls/ItemCumulEntity");                                       // Récupère les noeuds enfants ItemCumulEntity de nodeCurrent
                XmlNodeList l_uCodeLinked = nodeCurrent.SelectNodes("LinkedItems/ItemLinkedEntity");                                    // Récupère les noeuds enfants ItemLinkedEntity de nodeCurrent

                if (l_uCodeCumul.Count == 0)                                                                                            // Si il n'y a pas de noeuds enfants :
                {
                    cumuls.Add(new Cumul(codeFillingMethod, cumulType, fillingMethod, formula, sign, uCodeCumul));                      // On ajoute une liste de cumuls vide
                }

                else
                {
                    foreach (XmlNode cumul in l_uCodeCumul)                                                                             // Sinon pour chaque cumul dans la liste :
                    {
                        XmlNode n_codeFillingMethod = cumul.FirstChild;                                                                 // Crée un noeud avec la première propriété du noeud parcourue dans l_uCodeCumul
                        XmlNode n_cumulType = n_codeFillingMethod.NextSibling;                                                          
                        XmlNode n_fillingMethod = n_cumulType.NextSibling;
                        XmlNode n_formula = n_fillingMethod.NextSibling;
                        XmlNode n_sign = n_formula.NextSibling;
                        XmlNode n_uCodeCumul = cumul.LastChild;

                        codeFillingMethod = n_codeFillingMethod.InnerText;                                                              // Récupère la valeur de CodeFillingMethod
                        cumulType = n_cumulType.InnerText;
                        fillingMethod = n_fillingMethod.InnerText;
                        formula = n_formula.InnerText;
                        sign = n_sign.InnerText;
                        uCodeCumul = n_uCodeCumul.Attributes["ExportCode"].Value;                                                       // Récupère le code du cumul

                        cumuls.Add(new Cumul(codeFillingMethod, cumulType, fillingMethod, formula, sign, uCodeCumul));                  // Ajoute un cumul a la liste des cumuls
                    }
                }         

                if(l_uCodeLinked.Count == 0)                                                                                            // Si il n'y a pas d'élements dans l_uCodeLinked :
                {
                    linkedItemLabel = "";                                                                                               // linkedItemLabel est nul
                }       

                else
                {
                    foreach(XmlNode linked in l_uCodeLinked)                                                                            // Sinon pour chaque noeud de l_uCodeLinked
                    {
                        XmlNode n_linked = linked.FirstChild;
                        linkedItemLabel = n_linked.Attributes["ExportCode"].Value;                                                      // Récupère la valeur de l'attribut du noeud courant
                    }
                }


                // Récupère toutes les valeurs des noeuds ou des attributs choisis

                type = n_type.InnerText;                                                                                                
                sousType = n_sousType.InnerText;
                exportCode = GetAttribute(n_exportCode, "ExportCategory");
                exportDescription = GetAttribute(n_exportDescription, "ExportDescription");
                exportLabel = GetAttribute(n_exportLabel, "ExportCode");
                displayColumn = n_displayColumn.InnerText;
                employeeAmount = n_employeeAmount.InnerText;
                employeeBasis = n_employeebasis.InnerText;
                employeeRate = n_employeeRate.InnerText;
                natureUCodeExportLabel = GetAttribute(n_natureUCode, "ExportLabel");
                subNatureUCodeExportLabel = GetAttribute(n_subNature, "ExportCode");

                activityType = n_activityType.InnerText;
                attachedPeriod = n_attachedperiod.InnerText;
                expenseAccount = n_expenseAccount.InnerText;
                expensesStateDoNotCheckRate = n_expenseState.InnerText;
                isBonus = n_isBonus.InnerText;
                isInWelcome = n_isInWelcome.InnerText;
                isPeriodic = n_isPeriodic.InnerText;
                organismeUCode = GetAttribute(n_organismeUCode, "ExportCode");
                paymentPeriodicity = n_paymentPeriodicity.InnerText;
                regimeUCode = GetAttribute(n_regimeUCode, "ExportCode");
                uCodeAbsence = GetAttribute(n_uCodeAbsence, "ExportCode");
                uCodeGroup = GetAttribute(n_uCodeGroup, "ExportCode");

                onlyInPilote = n_onlyInPilote.InnerText;
                advancedMode = n_advancedMode.InnerText;
                advanceType = n_advanceType.InnerText;
                canBeNotConcerned = n_canBeNotConcerned.InnerText;
                employerAmount = n_employerAmount.InnerText;
                employerBasis = n_employerBasis.InnerText;
                employerRate = n_employerRate.InnerText;
                hasItemLinked = n_hasItemLinked.InnerText;
                hasSickLeaveComplement = n_hasSickLeaveComplement.InnerText;
                notInAccountingTransfert = n_notInAccountingTransfert.InnerText;
                themeId = n_themeId.InnerText;

                contractId = GetAttribute(n_contractId, "ExportCode");
                ccnParam = n_ccnParam.InnerText;
                printCodeFlag = n_printCodeFlag.InnerText;
                printCondition = n_printCondition.InnerText;

                january = l_january[i].InnerText;
                february = l_february[i].InnerText;
                march = l_march[i].InnerText;
                april = l_april[i].InnerText;
                may = l_may[i].InnerText;
                june = l_june[i].InnerText;
                july = l_july[i].InnerText;
                august = l_august[i].InnerText;
                september = l_september[i].InnerText;
                october = l_october[i].InnerText;
                november = l_november[i].InnerText;
                december = l_december[i].InnerText;

                List<String> l_periodicity = new List<String>();                                                                    // Crée une liste contenant les périodes

                l_periodicity.Add(january);                                                                                         // Ajoute les périodes a la liste de périodes
                l_periodicity.Add(february);
                l_periodicity.Add(march);
                l_periodicity.Add(april);
                l_periodicity.Add(may);
                l_periodicity.Add(june);
                l_periodicity.Add(july);
                l_periodicity.Add(august);
                l_periodicity.Add(september);
                l_periodicity.Add(october);
                l_periodicity.Add(november);
                l_periodicity.Add(december);

                // Crée une rubrique détaillée contenant tout les noeuds choisis ainsi que les périodes, cumuls et item liés
                Detail det = new Detail(sousType, type, exportCode, exportDescription, exportLabel, displayColumn, employeeAmount, employeeBasis, employeeRate, natureUCodeExportLabel, subNatureUCodeExportLabel, activityType, attachedPeriod,
                    expenseAccount, expensesStateDoNotCheckRate, isBonus, isInWelcome, isPeriodic, organismeUCode, paymentPeriodicity, regimeUCode, uCodeAbsence, uCodeGroup, onlyInPilote, advancedMode, advanceType,
                    canBeNotConcerned, employerAmount, employerBasis, employerRate, hasItemLinked, hasSickLeaveComplement, notInAccountingTransfert, themeId, contractId, ccnParam, printCodeFlag, printCondition, linkedItemLabel, l_periodicity, cumuls);

                // Ajoute la rubrique crée a la liste des rubriques
                l_rubriques.Add(det);       
            }

            // Parser.cs retourne la liste des rubriques du fichier
            return l_rubriques;
        }
    }
}
