﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cumuls
{
    public partial class Criteres : Form
    {
        private readonly Recup _recup;
        private int nb_rubriques = 0;
        public void GetItems()
        {
            // Liste des paramètres par défaut : critiques, neutres et inutiles
            string[] arr_critique = { "Type", "Sous Type", "Nature", "Sous Nature", "ContractId" };
            string[] arr_mid = { "ActivityType", "AttachedPeriod", "ExpenseAccount", "ExpensesStateDoNotCheckRate", "IsBonus", "IsInWelcome","IsPeriodic","OrganismeUCode",
                "PaymentPeriodicity","RegimeUCode", "UCodeAbsence","UCodeGroup", "OnlyInPilote", "AdvancedMode", "AdvanceType", "CanBeNotConcerned", "EmployerAmount",
                "EmployerBasis", "EmployerRate"};
            string[] arr_fac = { "HasItemLinked", "HasSickLeaveComplement", "NotInAccountingTransfert", "ThemeId", "CCNParam", "PrintCodeFlag", "PrintCondition" };

            checkedListBox_critique.Items.AddRange(arr_critique);                                                                           // Ajoute arr_critique a la checkedListBox des paramètres critiques
            checkedListBox_choix.Items.AddRange(arr_mid);                                                                                   // Ajoute arr_mid a la checkedListBox des paramètres neutres
            checkedListBox_inutile.Items.AddRange(arr_fac);                                                                                 // Ajoute arr_fac a la checkedListBox des paramètres inutiles
        }

        private string GetItemNumber(CheckedListBox to_count)                                                                               // Méthode retournant le nombre d'items dans une checkedListBox
        {
            int nb = 0;
            string mess = "Items : ";
            foreach(Object check in to_count.Items)
            {
                nb++;
            }

            return mess + nb;
        }

        public Criteres(Recup _recup, int p_nb_rubriques)
        {
            InitializeComponent();
            this._recup = _recup;                                                                                                           // Récupère l'instance de Recup.cs et la stock dans _recup
            GetItems();                                                                                                                     // Rempli les checkedListBox avec les valeurs pas défaut
            nb_rubriques = p_nb_rubriques;

            label_nbcrit.Text = GetItemNumber(checkedListBox_critique).ToString();                                                          //
            label_nbchoix.Text = GetItemNumber(checkedListBox_choix).ToString();                                                            // Récupère le nombre d'items dans chaque checkedListBox
            label_nbinu.Text = GetItemNumber(checkedListBox_inutile).ToString();                                                            //
        }

        private void button_choix_to_fac_Click(object sender, EventArgs e)
        {
            List<Object> temp = new List<Object>();
            List<Object> temp2 = new List<Object>();

            foreach(Object item in checkedListBox_choix.CheckedItems)
            {
                temp.Add(item);
            }

            foreach(Object item in checkedListBox_critique.CheckedItems)
            {
                temp2.Add(item);
            }

            foreach(Object item in temp)
            {
                checkedListBox_inutile.Items.Add(item);
            }

            foreach (Object item in temp2)
            {
                checkedListBox_inutile.Items.Add(item);
            }

            foreach(Object item in temp)
            {
                checkedListBox_choix.Items.Remove(item);
            }

            foreach(Object item in temp2)
            {
                checkedListBox_critique.Items.Remove(item);
            }

            label_nbcrit.Text = GetItemNumber(checkedListBox_critique).ToString();
            label_nbchoix.Text = GetItemNumber(checkedListBox_choix).ToString();
            label_nbinu.Text = GetItemNumber(checkedListBox_inutile).ToString();
        }

        private void button_fac_to_choix_Click(object sender, EventArgs e)
        {
            List<Object> temp = new List<Object>();
            List<Object> temp2 = new List<Object>();

            foreach (Object item in checkedListBox_inutile.CheckedItems)
            {
                temp.Add(item);              
            }

            foreach(Object item in checkedListBox_critique.CheckedItems)
            {
                temp2.Add(item);
            }

            foreach(Object item in temp)
            {
                checkedListBox_choix.Items.Add(item);
            }

            foreach (Object item in temp2)
            {
                checkedListBox_choix.Items.Add(item);              
            }

            foreach(Object item in temp)
            {
                checkedListBox_inutile.Items.Remove(item);
            }

            foreach(Object item in temp2)
            {
                checkedListBox_critique.Items.Remove(item);
            }

            label_nbcrit.Text = GetItemNumber(checkedListBox_critique).ToString();
            label_nbchoix.Text = GetItemNumber(checkedListBox_choix).ToString();
            label_nbinu.Text = GetItemNumber(checkedListBox_inutile).ToString();
        }

        private void button_choix_to_crit_Click(object sender, EventArgs e)
        {
            List<Object> temp = new List<Object>();
            List<Object> temp2 = new List<Object>();

            foreach (Object item in checkedListBox_choix.CheckedItems)
            {
                temp.Add(item);   
            }

            foreach(Object item in checkedListBox_inutile.CheckedItems)
            {
                temp2.Add(item);               
            }

            foreach(Object item in temp)
            {
                checkedListBox_critique.Items.Add(item);
            }

            foreach (Object item in temp2)
            {
                checkedListBox_critique.Items.Add(item);  
            }

            foreach(Object item in temp)
            {
                checkedListBox_choix.Items.Remove(item);
            }

            foreach(Object item in temp2)
            {
                checkedListBox_inutile.Items.Remove(item);
            }

            label_nbcrit.Text = GetItemNumber(checkedListBox_critique).ToString();
            label_nbchoix.Text = GetItemNumber(checkedListBox_choix).ToString();
            label_nbinu.Text = GetItemNumber(checkedListBox_inutile).ToString();
        }

        private void button_crit_to_choix_Click(object sender, EventArgs e)
        {
            List<Object> temp = new List<Object>();
            List<Object> temp2 = new List<Object>();

            foreach (Object item in checkedListBox_critique.CheckedItems)
            {
                temp.Add(item);             
            }

            foreach(Object item in checkedListBox_inutile.CheckedItems)
            {
                temp2.Add(item);               
            }

            foreach(Object item in temp)
            {
                checkedListBox_choix.Items.Add(item);
            }

            foreach (Object item in temp2)
            {
                checkedListBox_choix.Items.Add(item);                
            }

            foreach(Object item in temp)
            {
                checkedListBox_critique.Items.Remove(item);
            }

            foreach(Object item in temp2)
            {
                checkedListBox_inutile.Items.Remove(item);
            }

            label_nbcrit.Text = GetItemNumber(checkedListBox_critique).ToString();
            label_nbchoix.Text = GetItemNumber(checkedListBox_choix).ToString();
            label_nbinu.Text = GetItemNumber(checkedListBox_inutile).ToString();
        }

        private void button_reset_Click(object sender, EventArgs e)
        {
            checkedListBox_critique.Items.Clear();
            checkedListBox_choix.Items.Clear();                                                                                 // Vide toutes les checkedListBox
            checkedListBox_inutile.Items.Clear();

            textBox_nberreur.Text = "";                                                                                         // Vide la textBox contenant le nombre d'erreurs tolérées

            GetItems();                                                                                                         // Rempli les checkedListBox avec les valeurs par défaut
        }

        private void button_regroupement_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<Regroupement>().Any())                                                             // Si une instance de Regroupement.cs est ouverte :
            {
                Application.OpenForms.OfType<Regroupement>().First().Close();                                                   // On ferme l'instance
            }

            List<string> crit = new List<string>();                                                                             // Liste contenant les paramètres critiques
            List<string> choix = new List<string>();                                                                            // Liste contenant les paramètres neutres
            List<string> inu = new List<string>();                                                                              // Liste contenant les paramètres inutiles
            int nb_erreurs = 0;

            foreach (Object item in checkedListBox_critique.Items)
            {
                string value = item.ToString();
                crit.Add(value);                                                                                                // Ajoute les paramètres critiques à la liste
            }

            foreach (Object item in checkedListBox_choix.Items)
            {
                string value = item.ToString();
                choix.Add(value);                                                                                               // Ajoute les paramètres neutres à la liste
            }

            foreach (Object item in checkedListBox_inutile.Items)
            {
                string value = item.ToString();
                inu.Add(value);                                                                                                 // Ajoute les paramètres inutiles à la liste
            }

            if(textBox_nberreur.Text == "" || textBox_nberreur.Text == null)                                                    // Si le nombre d'erreurs tolérées n'est pas saisie
            {
                string messageError = "Nombre d'erreurs tolérées non saisi\nAppuyez sur OK pour continuer avec 0 erreurs tolérée\nAppuyez sur Annuler pour saisir un nombre d'erreurs";
                string titreError = "Erreur";
                MessageBoxButtons boutonsError = MessageBoxButtons.OKCancel;
                MessageBoxIcon error = MessageBoxIcon.Error;
                DialogResult resError = MessageBox.Show(messageError, titreError, boutonsError, error);

                if(resError == DialogResult.OK)
                {
                    nb_erreurs = 0;                                                                                             // Affecte le nombre d'erreurs tolérées a 0 par défaut si OK est appuyé
                }

                else
                {
                    return;                                                                                                     // Sinon, stop l'opération
                }         
            }

            else
            {
                nb_erreurs = Convert.ToInt32(textBox_nberreur.Text);                                                            // Sinon nb_erreurs prend la valeur saisie dans la textBox
            }

            bool use_cumul = false;
            if (checkBox_no.Checked)
            {
                use_cumul = true;
            }

            Dictionary<Item, List<Item>> dico_commun = new Dictionary<Item, List<Item>>();                                      // Crée une instance de dictionnaire
            dico_commun = this._recup.Commun(crit, choix, inu, nb_erreurs, use_cumul);                                                     // Crée un dictionnaire avec la méthode Commun dans Recup.cs

            Regroupement rgp = new Regroupement(dico_commun, nb_rubriques);                                                                   // Crée une instance de Regroupement.cs avec en paramètre le dictionnaire
            rgp.Show();
        }

        private void textBox_nberreur_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))                                                         // Bloque la saisie des cacractères autre que 0123456789
            {
                e.Handled = true;
            }
        }

        private void button_allcrit_Click(object sender, EventArgs e)                                                           // Méthode permettant de sélectionner/déselctionner tous les items de la checklistbox
        {
            for(int i = 0; i < checkedListBox_critique.Items.Count; i++)                                                        // Pour tous les items présents dans la checkListBox :
            {
                if(checkedListBox_critique.GetItemCheckState(i) == CheckState.Checked)                                          // Si l'item est checked :
                {
                    checkedListBox_critique.SetItemChecked(i, false);                                                           // L'item repasse unckecked
                }
                else                                                                                                            // Sinon :
                {   
                    checkedListBox_critique.SetItemChecked(i, true);                                                            // L'item passe checked
                }
                
            }
        }

        private void button_allchoix_Click(object sender, EventArgs e)
        {
            for(int i = 0; i < checkedListBox_choix.Items.Count; i++)
            {
                if(checkedListBox_choix.GetItemCheckState(i) == CheckState.Checked)
                {
                    checkedListBox_choix.SetItemChecked(i, false);
                }
                else
                {
                    checkedListBox_choix.SetItemChecked(i, true);
                }               
            }
        }

        private void button_allinu_Click(object sender, EventArgs e)
        {
            for(int i = 0; i < checkedListBox_inutile.Items.Count; i++)
            {
                if(checkedListBox_inutile.GetItemCheckState(i) == CheckState.Checked)
                {
                    checkedListBox_inutile.SetItemChecked(i, false);
                }
                else
                {
                    checkedListBox_inutile.SetItemChecked(i, true);
                }             
            }
        }
    }
}
