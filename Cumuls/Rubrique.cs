﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cumuls
{
    class Rubrique
    {
        private String sousType;
        private String type;
        private String exportCategory;
        private String exportDescription;
        private String exportLabel;
        private String displayColumn;
        private String employeeAmount;
        private String employeeBasis;
        private String employeeRate;
        private String natureUCodeExportLabel;
        private String subNatureUCodeExportLabel;

        private String activityType;
        private String attachedPeriod;
        private String expenseAccount;
        private String expensesStateDoNotCheckRate;
        private String isBonus;
        private String isInWelcome;
        private String isPeriodic;
        private String organismeUCode;
        private String paymentPeriodicity;
        private String regimeUCode;
        private String uCodeAbsence;
        private String uCodeGroup;

        private String onlyInPilote;
        private String advancedMode;
        private String advanceType;
        private String canBeNotConcerned;
        private String employerAmount;
        private String employerBasis;
        private String employerRate;
        private String hasItemLinked;
        private String hasSickLeaveComplement;
        private String notInAccountingTransfert;
        private String themeId;

        private String contractId;
        private String ccnParam;
        private String printCodeFlag;
        private String printCondition;

            public Rubrique(String p_sousType, String p_type, String p_exportCategory, String p_exportDescription, String p_exportLabel, String p_displayColumn,
            String p_employeeAmount, String p_employeeBasis, String p_employeeRate, String p_natureUCodeExportLabel, String p_subNatureUCodeExportLabel, String p_activityType,
        String p_attachedPeriod, String p_expenseAccount, String p_expensesStateDoNotCheckRate, String p_isBonus, String p_isInWelcome, String p_isPeriodic, String p_organismeUCode,
        String p_paymentPeriodicity, String p_regimeUCode, String p_uCodeAbsence, String p_uCodeGroup, String p_onlyInPilote, String p_advancedMode, String p_advanceType, String p_canBeNotConcerned,
        String p_employerAmount, String p_employerBasis, String p_employerRate, String p_hasItemLinked, String p_hasSickLeaveComplement, String p_notInAccountingTransfert, String p_themeId,
        String p_contractId, String p_ccnParam, String p_printCodeFlag, String p_printCondition)
        {
            sousType = p_sousType;
            type = p_type;
            exportCategory = p_exportCategory;
            exportDescription = p_exportDescription;
            exportLabel = p_exportLabel;
            displayColumn = p_displayColumn;
            employeeAmount = p_employeeAmount;
            employeeBasis = p_employeeBasis;
            employeeRate = p_employeeRate;
            natureUCodeExportLabel = p_natureUCodeExportLabel;
            subNatureUCodeExportLabel = p_subNatureUCodeExportLabel;

            activityType = p_activityType;
            attachedPeriod = p_attachedPeriod;
            expenseAccount = p_expenseAccount;
            expensesStateDoNotCheckRate = p_expensesStateDoNotCheckRate;
            isBonus = p_isBonus;
            isInWelcome = p_isInWelcome;
            isPeriodic = p_isPeriodic;
            organismeUCode = p_organismeUCode;
            paymentPeriodicity = p_paymentPeriodicity;
            regimeUCode = p_regimeUCode;
            uCodeAbsence = p_uCodeAbsence;
            uCodeGroup = p_uCodeGroup;

            onlyInPilote = p_onlyInPilote;
            advancedMode = p_advancedMode;
            advanceType = p_advanceType;
            canBeNotConcerned = p_canBeNotConcerned;
            employerAmount = p_employerAmount;
            employerBasis = p_employerBasis;
            employerRate = p_employerRate;
            hasItemLinked = p_hasItemLinked;
            hasSickLeaveComplement = p_hasSickLeaveComplement;
            notInAccountingTransfert = p_notInAccountingTransfert;
            themeId = p_themeId;

            contractId = p_contractId;
            ccnParam = p_ccnParam;
            printCodeFlag = p_printCodeFlag;
            printCondition = p_printCondition;
        }

        public virtual string SousType()
        {
            return sousType;
        }
        public virtual string Type()
        {
            return type;
        }

        public virtual string ExportCategory()
        {
            return exportCategory;
        }

        public virtual string ExportDescription()
        {
            return exportDescription;
        }

        public virtual string ExportLabel()
        {
            return exportLabel;

        }

        public virtual string DisplayColumn()
        {
            return displayColumn;
        }

        public virtual string EmployeeAmount()
        {
            return employeeAmount;
        }

        public virtual string EmployeeBasis()
        {
            return employeeBasis;
        }

        public virtual string EmployeeRate()
        {
            return employeeRate;
        }

        public virtual string NatureCodeExportLabel()
        {
            return natureUCodeExportLabel;
        }

        public virtual string SubNatureUCodeExportLabel()
        {
            return subNatureUCodeExportLabel;
        }

        public virtual string ActivityType()
        {
            return activityType;
        }

        public virtual string AttachedPeriod()
        {
            return attachedPeriod;
        }

        public virtual string ExpenseAccount()
        {
            return expenseAccount;
        }

        public virtual string ExpenseState()
        {
            return expensesStateDoNotCheckRate;
        }

        public virtual string IsBonus()
        {
            return isBonus;
        }
        public virtual string IsInWelcome()
        {
            return isInWelcome;
        }
        public virtual string IsPeriodic()
        {
            return isPeriodic;
        }
        public virtual string OrganismeUCode()
        {
            return organismeUCode;
        }
        public virtual string PaymentPeriodicity()
        {
            return paymentPeriodicity;
        }
        public virtual string RegimeUCode()
        {
            return regimeUCode;
        }
        public virtual string UCodeAbsence()
        {
            return uCodeAbsence;
        }
        public virtual string UCodeGroup()
        {
            return uCodeGroup;
        }
        public virtual string OnlyInPilote()
        {
            return onlyInPilote;
        }
        public virtual string AdvancedMode()
        {
            return advancedMode;
        }
        public virtual string AdvanceType()
        {
            return advanceType;
        }
        public virtual string CanBeNotConcerned()
        {
            return canBeNotConcerned;
        }
        public virtual string EmployerAmount()
        {
            return employerAmount;
        }
        public virtual string EmployerBasis()
        {
            return employerBasis;
        }
        public virtual string EmployerRate()
        {
            return employerRate;
        }
        public virtual string HasItemLinked()
        {
            return hasItemLinked;
        }
        public virtual string HasSickLeaveComplement()
        {
            return hasSickLeaveComplement;
        }
        public virtual string NotInAccountingTransfert()
        {
            return notInAccountingTransfert;
        }
        public virtual string ThemeId()
        {
            return themeId;
        }
        public virtual string ContractId()
        {
            return contractId;
        }
        public virtual string CcnParam()
        {
            return ccnParam;
        }
        public virtual string PrintCodeFlag()
        {
            return printCodeFlag;
        }
        public virtual string PrintCondition()
        {
            return printCondition;
        }
    }
}
