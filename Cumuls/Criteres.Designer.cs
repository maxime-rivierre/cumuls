﻿namespace Cumuls
{
    partial class Criteres
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.checkedListBox_critique = new System.Windows.Forms.CheckedListBox();
            this.checkedListBox_choix = new System.Windows.Forms.CheckedListBox();
            this.checkedListBox_inutile = new System.Windows.Forms.CheckedListBox();
            this.button_inu = new System.Windows.Forms.Button();
            this.button_neutre2 = new System.Windows.Forms.Button();
            this.button_crit = new System.Windows.Forms.Button();
            this.b_neutre = new System.Windows.Forms.Button();
            this.label_crit = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button_info_crit = new System.Windows.Forms.Button();
            this.button_info_neutre = new System.Windows.Forms.Button();
            this.button_info_inutile = new System.Windows.Forms.Button();
            this.button_reset = new System.Windows.Forms.Button();
            this.textBox_nberreur = new System.Windows.Forms.TextBox();
            this.label_nberreur = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
            this.button_regroupement = new System.Windows.Forms.Button();
            this.button_allcrit = new System.Windows.Forms.Button();
            this.button_allchoix = new System.Windows.Forms.Button();
            this.button_allinu = new System.Windows.Forms.Button();
            this.label_nbcrit = new System.Windows.Forms.Label();
            this.label_nbchoix = new System.Windows.Forms.Label();
            this.label_nbinu = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox_no = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // checkedListBox_critique
            // 
            this.checkedListBox_critique.CheckOnClick = true;
            this.checkedListBox_critique.FormattingEnabled = true;
            this.checkedListBox_critique.Location = new System.Drawing.Point(23, 123);
            this.checkedListBox_critique.Name = "checkedListBox_critique";
            this.checkedListBox_critique.Size = new System.Drawing.Size(250, 289);
            this.checkedListBox_critique.TabIndex = 0;
            // 
            // checkedListBox_choix
            // 
            this.checkedListBox_choix.CheckOnClick = true;
            this.checkedListBox_choix.FormattingEnabled = true;
            this.checkedListBox_choix.Location = new System.Drawing.Point(517, 123);
            this.checkedListBox_choix.Name = "checkedListBox_choix";
            this.checkedListBox_choix.Size = new System.Drawing.Size(281, 289);
            this.checkedListBox_choix.TabIndex = 1;
            // 
            // checkedListBox_inutile
            // 
            this.checkedListBox_inutile.CheckOnClick = true;
            this.checkedListBox_inutile.FormattingEnabled = true;
            this.checkedListBox_inutile.Location = new System.Drawing.Point(1027, 123);
            this.checkedListBox_inutile.Name = "checkedListBox_inutile";
            this.checkedListBox_inutile.Size = new System.Drawing.Size(250, 289);
            this.checkedListBox_inutile.TabIndex = 2;
            // 
            // button_inu
            // 
            this.button_inu.Location = new System.Drawing.Point(820, 220);
            this.button_inu.Name = "button_inu";
            this.button_inu.Size = new System.Drawing.Size(150, 25);
            this.button_inu.TabIndex = 3;
            this.button_inu.Text = "Ne pas vérifier";
            this.button_inu.UseVisualStyleBackColor = true;
            this.button_inu.Click += new System.EventHandler(this.button_choix_to_fac_Click);
            // 
            // button_neutre2
            // 
            this.button_neutre2.Location = new System.Drawing.Point(820, 273);
            this.button_neutre2.Name = "button_neutre2";
            this.button_neutre2.Size = new System.Drawing.Size(150, 25);
            this.button_neutre2.TabIndex = 4;
            this.button_neutre2.Text = "Rendre neutre";
            this.button_neutre2.UseVisualStyleBackColor = true;
            this.button_neutre2.Click += new System.EventHandler(this.button_fac_to_choix_Click);
            // 
            // button_crit
            // 
            this.button_crit.Location = new System.Drawing.Point(333, 220);
            this.button_crit.Name = "button_crit";
            this.button_crit.Size = new System.Drawing.Size(150, 25);
            this.button_crit.TabIndex = 1;
            this.button_crit.Text = "Rendre critique";
            this.button_crit.UseVisualStyleBackColor = true;
            this.button_crit.Click += new System.EventHandler(this.button_choix_to_crit_Click);
            // 
            // b_neutre
            // 
            this.b_neutre.Location = new System.Drawing.Point(333, 273);
            this.b_neutre.Name = "b_neutre";
            this.b_neutre.Size = new System.Drawing.Size(150, 25);
            this.b_neutre.TabIndex = 2;
            this.b_neutre.Text = "Rendre neutre";
            this.b_neutre.UseVisualStyleBackColor = true;
            this.b_neutre.Click += new System.EventHandler(this.button_crit_to_choix_Click);
            // 
            // label_crit
            // 
            this.label_crit.AutoSize = true;
            this.label_crit.Location = new System.Drawing.Point(103, 62);
            this.label_crit.Name = "label_crit";
            this.label_crit.Size = new System.Drawing.Size(90, 13);
            this.label_crit.TabIndex = 7;
            this.label_crit.Text = "Critères critiques :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1096, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Critères non-regardés :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(614, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Critères neutres :";
            // 
            // button_info_crit
            // 
            this.button_info_crit.Image = global::Cumuls.Properties.Resources.info1;
            this.button_info_crit.Location = new System.Drawing.Point(71, 56);
            this.button_info_crit.Name = "button_info_crit";
            this.button_info_crit.Size = new System.Drawing.Size(26, 24);
            this.button_info_crit.TabIndex = 10;
            this.button_info_crit.TabStop = false;
            this.toolTip1.SetToolTip(this.button_info_crit, "Ces critères sont considérés comme critiques.\r\nSi un de ces critères n\'est pas co" +
        "mmun a deux rubriques,\r\nelles ne pourront pas être regroupées ensemble.");
            this.button_info_crit.UseVisualStyleBackColor = true;
            // 
            // button_info_neutre
            // 
            this.button_info_neutre.Image = global::Cumuls.Properties.Resources.info1;
            this.button_info_neutre.Location = new System.Drawing.Point(582, 56);
            this.button_info_neutre.Name = "button_info_neutre";
            this.button_info_neutre.Size = new System.Drawing.Size(26, 24);
            this.button_info_neutre.TabIndex = 11;
            this.button_info_neutre.TabStop = false;
            this.toolTip2.SetToolTip(this.button_info_neutre, "Ces critères sont par défaut considérés comme neutres.\r\nIls permettent d\'ajuster " +
        "la précision du regroupement.\r\nLe nombre d\'erreurs tolérées s\'applique sur ces c" +
        "ritères.");
            this.button_info_neutre.UseVisualStyleBackColor = true;
            // 
            // button_info_inutile
            // 
            this.button_info_inutile.Image = global::Cumuls.Properties.Resources.info1;
            this.button_info_inutile.Location = new System.Drawing.Point(1064, 56);
            this.button_info_inutile.Name = "button_info_inutile";
            this.button_info_inutile.Size = new System.Drawing.Size(26, 24);
            this.button_info_inutile.TabIndex = 12;
            this.button_info_inutile.TabStop = false;
            this.toolTip3.SetToolTip(this.button_info_inutile, "Ces critères sont considérés comme inutiles.\r\nIls ne seront pas pris en compte au" +
        " moment du regroupement.");
            this.button_info_inutile.UseVisualStyleBackColor = true;
            // 
            // button_reset
            // 
            this.button_reset.Location = new System.Drawing.Point(517, 575);
            this.button_reset.Name = "button_reset";
            this.button_reset.Size = new System.Drawing.Size(281, 23);
            this.button_reset.TabIndex = 9;
            this.button_reset.Text = "Reset";
            this.button_reset.UseVisualStyleBackColor = true;
            this.button_reset.Click += new System.EventHandler(this.button_reset_Click);
            // 
            // textBox_nberreur
            // 
            this.textBox_nberreur.Location = new System.Drawing.Point(684, 484);
            this.textBox_nberreur.Name = "textBox_nberreur";
            this.textBox_nberreur.Size = new System.Drawing.Size(114, 20);
            this.textBox_nberreur.TabIndex = 8;
            this.textBox_nberreur.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_nberreur_KeyPress);
            // 
            // label_nberreur
            // 
            this.label_nberreur.AutoSize = true;
            this.label_nberreur.Location = new System.Drawing.Point(514, 491);
            this.label_nberreur.Name = "label_nberreur";
            this.label_nberreur.Size = new System.Drawing.Size(133, 13);
            this.label_nberreur.TabIndex = 15;
            this.label_nberreur.Text = "Nombre d\'erreurs tolérées :";
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 15000;
            this.toolTip1.InitialDelay = 100;
            this.toolTip1.ReshowDelay = 100;
            // 
            // toolTip2
            // 
            this.toolTip2.AutoPopDelay = 15000;
            this.toolTip2.InitialDelay = 100;
            this.toolTip2.ReshowDelay = 100;
            // 
            // toolTip3
            // 
            this.toolTip3.AutoPopDelay = 15000;
            this.toolTip3.InitialDelay = 100;
            this.toolTip3.ReshowDelay = 100;
            // 
            // button_regroupement
            // 
            this.button_regroupement.Location = new System.Drawing.Point(517, 614);
            this.button_regroupement.Name = "button_regroupement";
            this.button_regroupement.Size = new System.Drawing.Size(281, 23);
            this.button_regroupement.TabIndex = 10;
            this.button_regroupement.Text = "Regroupement";
            this.button_regroupement.UseVisualStyleBackColor = true;
            this.button_regroupement.Click += new System.EventHandler(this.button_regroupement_Click);
            // 
            // button_allcrit
            // 
            this.button_allcrit.Location = new System.Drawing.Point(23, 418);
            this.button_allcrit.Name = "button_allcrit";
            this.button_allcrit.Size = new System.Drawing.Size(250, 23);
            this.button_allcrit.TabIndex = 5;
            this.button_allcrit.Text = "Sélectionner tous les items critiques";
            this.button_allcrit.UseVisualStyleBackColor = true;
            this.button_allcrit.Click += new System.EventHandler(this.button_allcrit_Click);
            // 
            // button_allchoix
            // 
            this.button_allchoix.Location = new System.Drawing.Point(517, 418);
            this.button_allchoix.Name = "button_allchoix";
            this.button_allchoix.Size = new System.Drawing.Size(281, 23);
            this.button_allchoix.TabIndex = 6;
            this.button_allchoix.Text = "Sélectionner tous les items neutres";
            this.button_allchoix.UseVisualStyleBackColor = true;
            this.button_allchoix.Click += new System.EventHandler(this.button_allchoix_Click);
            // 
            // button_allinu
            // 
            this.button_allinu.Location = new System.Drawing.Point(1027, 418);
            this.button_allinu.Name = "button_allinu";
            this.button_allinu.Size = new System.Drawing.Size(250, 23);
            this.button_allinu.TabIndex = 7;
            this.button_allinu.Text = "Sélectionner tous les items non pris en compte";
            this.button_allinu.UseVisualStyleBackColor = true;
            this.button_allinu.Click += new System.EventHandler(this.button_allinu_Click);
            // 
            // label_nbcrit
            // 
            this.label_nbcrit.AutoSize = true;
            this.label_nbcrit.Location = new System.Drawing.Point(131, 95);
            this.label_nbcrit.Name = "label_nbcrit";
            this.label_nbcrit.Size = new System.Drawing.Size(35, 13);
            this.label_nbcrit.TabIndex = 16;
            this.label_nbcrit.Text = "label3";
            // 
            // label_nbchoix
            // 
            this.label_nbchoix.AutoSize = true;
            this.label_nbchoix.Location = new System.Drawing.Point(640, 95);
            this.label_nbchoix.Name = "label_nbchoix";
            this.label_nbchoix.Size = new System.Drawing.Size(35, 13);
            this.label_nbchoix.TabIndex = 17;
            this.label_nbchoix.Text = "label4";
            // 
            // label_nbinu
            // 
            this.label_nbinu.AutoSize = true;
            this.label_nbinu.Location = new System.Drawing.Point(1135, 95);
            this.label_nbinu.Name = "label_nbinu";
            this.label_nbinu.Size = new System.Drawing.Size(35, 13);
            this.label_nbinu.TabIndex = 18;
            this.label_nbinu.Text = "label5";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(514, 532);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(235, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Prendre en compte les cumuls (Non par défaut) :";
            // 
            // checkBox_no
            // 
            this.checkBox_no.AutoSize = true;
            this.checkBox_no.Location = new System.Drawing.Point(756, 532);
            this.checkBox_no.Name = "checkBox_no";
            this.checkBox_no.Size = new System.Drawing.Size(42, 17);
            this.checkBox_no.TabIndex = 21;
            this.checkBox_no.Text = "Oui";
            this.checkBox_no.UseVisualStyleBackColor = true;
            // 
            // Criteres
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1314, 651);
            this.Controls.Add(this.checkBox_no);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label_nbinu);
            this.Controls.Add(this.label_nbchoix);
            this.Controls.Add(this.label_nbcrit);
            this.Controls.Add(this.button_allinu);
            this.Controls.Add(this.button_allchoix);
            this.Controls.Add(this.button_allcrit);
            this.Controls.Add(this.button_regroupement);
            this.Controls.Add(this.label_nberreur);
            this.Controls.Add(this.textBox_nberreur);
            this.Controls.Add(this.button_reset);
            this.Controls.Add(this.button_info_inutile);
            this.Controls.Add(this.button_info_neutre);
            this.Controls.Add(this.button_info_crit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_crit);
            this.Controls.Add(this.b_neutre);
            this.Controls.Add(this.button_crit);
            this.Controls.Add(this.button_neutre2);
            this.Controls.Add(this.button_inu);
            this.Controls.Add(this.checkedListBox_inutile);
            this.Controls.Add(this.checkedListBox_choix);
            this.Controls.Add(this.checkedListBox_critique);
            this.MaximizeBox = false;
            this.Name = "Criteres";
            this.Text = "Criteres";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox checkedListBox_critique;
        private System.Windows.Forms.CheckedListBox checkedListBox_choix;
        private System.Windows.Forms.CheckedListBox checkedListBox_inutile;
        private System.Windows.Forms.Button button_inu;
        private System.Windows.Forms.Button button_neutre2;
        private System.Windows.Forms.Button button_crit;
        private System.Windows.Forms.Button b_neutre;
        private System.Windows.Forms.Label label_crit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_info_crit;
        private System.Windows.Forms.Button button_info_neutre;
        private System.Windows.Forms.Button button_info_inutile;
        private System.Windows.Forms.Button button_reset;
        private System.Windows.Forms.TextBox textBox_nberreur;
        private System.Windows.Forms.Label label_nberreur;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.ToolTip toolTip3;
        private System.Windows.Forms.Button button_regroupement;
        private System.Windows.Forms.Button button_allcrit;
        private System.Windows.Forms.Button button_allchoix;
        private System.Windows.Forms.Button button_allinu;
        private System.Windows.Forms.Label label_nbcrit;
        private System.Windows.Forms.Label label_nbchoix;
        private System.Windows.Forms.Label label_nbinu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBox_no;
    }
}