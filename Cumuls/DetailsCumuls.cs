﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cumuls
{
    public partial class DetailsCumuls : Form
    {
        private DataTable dt_cumuls = new DataTable();                                                                                          // DataTable contenant les cumuls
        private DataTable dt_periodes = new DataTable();                                                                                        // DataTable contenant les périodes
        private string type;
        private string sousType;
              
        public DetailsCumuls(DataTable p_dt_cumuls, DataTable p_dt_periodes, String p_type, String p_sousType)
        {
            InitializeComponent();
            dt_cumuls = p_dt_cumuls;
            dt_periodes = p_dt_periodes;
            type = p_type;
            sousType = p_sousType;
            label_type_value.Text = type;
            label_soustype_value.Text = p_sousType;

            if(dt_cumuls.Rows.Count == 1)
            {
                DataRow first_row = dt_cumuls.Rows[0];
                string first_code = first_row["CodeCumulLabel"].ToString();

                if(first_code == "" || first_code == null)
                {
                    this.dataGridView_periodes.Location = new Point(70, 200);
                    this.label_type.Location = new Point(220, 150);
                    this.label_soustype.Location = new Point(550, 150);
                    this.label_type_value.Location = new Point(280, 150);
                    this.label_soustype_value.Location = new Point(630, 150);
                    this.Size = new Size(966, 350);
                    dataGridView_periodes.DataSource = dt_periodes;
                    dataGridView_periodes.Visible = true;
                    label_cumuls.Text = "Il n'y pas de cumuls disponible pour cette rubrique";
                    label_cumuls.Visible = true;
                }

                else
                {
                    dataGridView_cumuls.DataSource = dt_cumuls;
                    dataGridView_periodes.DataSource = dt_periodes;
                    dataGridView_cumuls.Visible = true;
                    dataGridView_periodes.Visible = true;
                    dataGridView_cumuls.Columns[6].Visible = false;
                    dataGridView_cumuls.Columns[7].Visible = false;
                    dataGridView_cumuls.Columns[8].Visible = false;
                }    
            }

            else
            {
                dataGridView_cumuls.DataSource = dt_cumuls;
                dataGridView_periodes.DataSource = dt_periodes;
                dataGridView_cumuls.Visible = true;
                dataGridView_periodes.Visible = true;
                dataGridView_cumuls.Columns[6].Visible = false;
                dataGridView_cumuls.Columns[7].Visible = false;
                dataGridView_cumuls.Columns[8].Visible = false;
            }
        }
    }
}
