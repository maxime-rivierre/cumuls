﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cumuls
{
    class Detail : Rubrique
    {
        private String uCodeLinkedItemExportLabel;                                                          // Label de l'item lié a la rubrique, si présent
        private List<String> l_dates;                                                                       // Liste contenant les 12 périodes
        private List<Cumul> l_cumuls;                                                                       // Liste de cumuls de la rubrique, si présents

        public Detail(String p_sousType, String p_type, String p_exportCategory, String p_exportDescription, String p_exportLabel, String p_displayColumn,
            String p_employeeAmount, String p_employeeBasis, String p_employeeRate, String p_uCodeExportlabel, String p_subNatureUCodeExportLabel, String p_activityType,
        String p_attachedPeriod, String p_expenseAccount, String p_expensesStateDoNotCheckRate, String p_isBonus, String p_isInWelcome, String p_isPeriodic, String p_organismeUCode,
        String p_paymentPeriodicity, String p_regimeUCode, String p_uCodeAbsence, String p_uCodeGroup, String p_onlyInPilote, String p_advancedMode, String p_advanceType, String p_canBeNotConcerned,
        String p_employerAmount, String p_employerBasis, String p_employerRate, String p_hasItemLinked, String p_hasSickLeaveComplement, String p_notInAccountingTransfert, String p_themeId,
        String p_contractId, String p_ccnParam, String p_printCodeFlag, String p_printCondition, String p_uCodeLinkedItemExportLabel, List<String> p_dates, List<Cumul> p_cumuls) 
            :base(p_sousType, p_type, p_exportCategory, p_exportDescription, p_exportLabel, p_displayColumn, p_employeeAmount,
            p_employeeBasis, p_employeeRate, p_uCodeExportlabel,  p_subNatureUCodeExportLabel, p_activityType, p_attachedPeriod, p_expenseAccount, p_expensesStateDoNotCheckRate, p_isBonus, p_isInWelcome,
            p_isPeriodic, p_organismeUCode, p_paymentPeriodicity, p_regimeUCode, p_uCodeAbsence,  p_uCodeGroup, p_onlyInPilote, p_advancedMode, p_advanceType, p_canBeNotConcerned,
        p_employerAmount, p_employerBasis, p_employerRate, p_hasItemLinked, p_hasSickLeaveComplement, p_notInAccountingTransfert, p_themeId, p_contractId, p_ccnParam, p_printCodeFlag, p_printCondition)
        {
            uCodeLinkedItemExportLabel = p_uCodeLinkedItemExportLabel;
            l_cumuls = p_cumuls;
            l_dates = p_dates; 
        }

        public string LinkedItemLabel()
        {
            return uCodeLinkedItemExportLabel;
        }

        public List<String> Dates()
        {
            return l_dates;
        }

        public List<Cumul> Cumuls()
        {
            return l_cumuls;
        }
    }
}
