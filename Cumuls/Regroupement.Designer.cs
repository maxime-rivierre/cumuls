﻿using System.Drawing;
using System.Windows.Forms;

namespace Cumuls
{
    partial class Regroupement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView_groupes = new System.Windows.Forms.DataGridView();
            this.dataGridView_communs = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label_nb_rubrique_groupe = new System.Windows.Forms.Label();
            this.label_nb_rubrique = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_groupes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_communs)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_groupes
            // 
            this.dataGridView_groupes.AllowUserToAddRows = false;
            this.dataGridView_groupes.AllowUserToDeleteRows = false;
            this.dataGridView_groupes.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(239)))), ((int)(((byte)(249)))));
            this.dataGridView_groupes.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_groupes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_groupes.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView_groupes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(25)))), ((int)(((byte)(72)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_groupes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.DarkTurquoise;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_groupes.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_groupes.EnableHeadersVisualStyles = false;
            this.dataGridView_groupes.Location = new System.Drawing.Point(49, 125);
            this.dataGridView_groupes.MultiSelect = false;
            this.dataGridView_groupes.Name = "dataGridView_groupes";
            this.dataGridView_groupes.ReadOnly = true;
            this.dataGridView_groupes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_groupes.Size = new System.Drawing.Size(525, 465);
            this.dataGridView_groupes.TabIndex = 0;
            this.dataGridView_groupes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_groupes_CellContentClick);
            // 
            // dataGridView_communs
            // 
            this.dataGridView_communs.AllowUserToAddRows = false;
            this.dataGridView_communs.AllowUserToDeleteRows = false;
            this.dataGridView_communs.AllowUserToOrderColumns = true;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(239)))), ((int)(((byte)(249)))));
            this.dataGridView_communs.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView_communs.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_communs.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView_communs.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(25)))), ((int)(((byte)(72)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_communs.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.DarkTurquoise;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_communs.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView_communs.EnableHeadersVisualStyles = false;
            this.dataGridView_communs.Location = new System.Drawing.Point(603, 125);
            this.dataGridView_communs.MultiSelect = false;
            this.dataGridView_communs.Name = "dataGridView_communs";
            this.dataGridView_communs.ReadOnly = true;
            this.dataGridView_communs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_communs.Size = new System.Drawing.Size(963, 465);
            this.dataGridView_communs.TabIndex = 1;
            this.dataGridView_communs.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(168, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(291, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Appuyez sur un groupe pour afficher les rubriques identiques";
            // 
            // label_nb_rubrique_groupe
            // 
            this.label_nb_rubrique_groupe.AutoSize = true;
            this.label_nb_rubrique_groupe.Location = new System.Drawing.Point(225, 53);
            this.label_nb_rubrique_groupe.Name = "label_nb_rubrique_groupe";
            this.label_nb_rubrique_groupe.Size = new System.Drawing.Size(35, 13);
            this.label_nb_rubrique_groupe.TabIndex = 3;
            this.label_nb_rubrique_groupe.Text = "label2";
            // 
            // label_nb_rubrique
            // 
            this.label_nb_rubrique.AutoSize = true;
            this.label_nb_rubrique.Location = new System.Drawing.Point(243, 22);
            this.label_nb_rubrique.Name = "label_nb_rubrique";
            this.label_nb_rubrique.Size = new System.Drawing.Size(35, 13);
            this.label_nb_rubrique.TabIndex = 4;
            this.label_nb_rubrique.Text = "label2";
            // 
            // Regroupement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1598, 627);
            this.Controls.Add(this.label_nb_rubrique);
            this.Controls.Add(this.label_nb_rubrique_groupe);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView_communs);
            this.Controls.Add(this.dataGridView_groupes);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Regroupement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Regroupement";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_groupes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_communs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_groupes;
        private DataGridView dataGridView_communs;
        private Label label1;
        private Label label_nb_rubrique_groupe;
        private Label label_nb_rubrique;
    }
}