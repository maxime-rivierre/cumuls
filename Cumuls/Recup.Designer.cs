﻿using System.Drawing;
using System.Windows.Forms;

namespace Cumuls
{
    partial class Recup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.textBox_chemin_archive = new System.Windows.Forms.TextBox();
            this.button_upload = new System.Windows.Forms.Button();
            this.label_chemin_archive = new System.Windows.Forms.Label();
            this.openFileDialog_uparchive = new System.Windows.Forms.OpenFileDialog();
            this.button_extract = new System.Windows.Forms.Button();
            this.folderBrowserDialog_upextract = new System.Windows.Forms.FolderBrowserDialog();
            this.button_fichier = new System.Windows.Forms.Button();
            this.openFileDialog_xml = new System.Windows.Forms.OpenFileDialog();
            this.button_parse = new System.Windows.Forms.Button();
            this.label_chemin_fichier = new System.Windows.Forms.Label();
            this.textBox_chemin_fichier = new System.Windows.Forms.TextBox();
            this.dataGridView_rubrique = new System.Windows.Forms.DataGridView();
            this.textBox_searchCategory = new System.Windows.Forms.TextBox();
            this.label_category = new System.Windows.Forms.Label();
            this.label_des = new System.Windows.Forms.Label();
            this.textBox_searchDes = new System.Windows.Forms.TextBox();
            this.label_lab = new System.Windows.Forms.Label();
            this.textBox_searchLabel = new System.Windows.Forms.TextBox();
            this.label_type = new System.Windows.Forms.Label();
            this.textBox_searchType = new System.Windows.Forms.TextBox();
            this.button_clear = new System.Windows.Forms.Button();
            this.button_critères = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_rubrique)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox_chemin_archive
            // 
            this.textBox_chemin_archive.Location = new System.Drawing.Point(282, 57);
            this.textBox_chemin_archive.Name = "textBox_chemin_archive";
            this.textBox_chemin_archive.ReadOnly = true;
            this.textBox_chemin_archive.Size = new System.Drawing.Size(563, 20);
            this.textBox_chemin_archive.TabIndex = 5;
            // 
            // button_upload
            // 
            this.button_upload.Location = new System.Drawing.Point(80, 56);
            this.button_upload.Name = "button_upload";
            this.button_upload.Size = new System.Drawing.Size(99, 23);
            this.button_upload.TabIndex = 1;
            this.button_upload.Text = "Choix archive";
            this.button_upload.UseVisualStyleBackColor = true;
            this.button_upload.Click += new System.EventHandler(this.button_upload_Click);
            // 
            // label_chemin_archive
            // 
            this.label_chemin_archive.AutoSize = true;
            this.label_chemin_archive.Location = new System.Drawing.Point(218, 61);
            this.label_chemin_archive.Name = "label_chemin_archive";
            this.label_chemin_archive.Size = new System.Drawing.Size(48, 13);
            this.label_chemin_archive.TabIndex = 7;
            this.label_chemin_archive.Text = "Chemin :";
            // 
            // openFileDialog_uparchive
            // 
            this.openFileDialog_uparchive.FileName = "openFileDialog_upload";
            // 
            // button_extract
            // 
            this.button_extract.Location = new System.Drawing.Point(80, 106);
            this.button_extract.Name = "button_extract";
            this.button_extract.Size = new System.Drawing.Size(99, 23);
            this.button_extract.TabIndex = 2;
            this.button_extract.Text = "Extraire vers";
            this.button_extract.UseVisualStyleBackColor = true;
            this.button_extract.Click += new System.EventHandler(this.button_extract_Click);
            // 
            // button_fichier
            // 
            this.button_fichier.Location = new System.Drawing.Point(80, 156);
            this.button_fichier.Name = "button_fichier";
            this.button_fichier.Size = new System.Drawing.Size(99, 23);
            this.button_fichier.TabIndex = 3;
            this.button_fichier.Text = "Choisir un fichier";
            this.button_fichier.UseVisualStyleBackColor = true;
            this.button_fichier.Click += new System.EventHandler(this.button_fichier_Click);
            // 
            // button_parse
            // 
            this.button_parse.Location = new System.Drawing.Point(80, 206);
            this.button_parse.Name = "button_parse";
            this.button_parse.Size = new System.Drawing.Size(99, 23);
            this.button_parse.TabIndex = 4;
            this.button_parse.Text = "Parcourir";
            this.button_parse.UseVisualStyleBackColor = true;
            this.button_parse.Click += new System.EventHandler(this.button_parse_Click);
            // 
            // label_chemin_fichier
            // 
            this.label_chemin_fichier.AutoSize = true;
            this.label_chemin_fichier.Location = new System.Drawing.Point(218, 161);
            this.label_chemin_fichier.Name = "label_chemin_fichier";
            this.label_chemin_fichier.Size = new System.Drawing.Size(48, 13);
            this.label_chemin_fichier.TabIndex = 8;
            this.label_chemin_fichier.Text = "Chemin :";
            // 
            // textBox_chemin_fichier
            // 
            this.textBox_chemin_fichier.Location = new System.Drawing.Point(282, 157);
            this.textBox_chemin_fichier.Name = "textBox_chemin_fichier";
            this.textBox_chemin_fichier.ReadOnly = true;
            this.textBox_chemin_fichier.Size = new System.Drawing.Size(563, 20);
            this.textBox_chemin_fichier.TabIndex = 6;
            // 
            // dataGridView_rubrique
            // 
            this.dataGridView_rubrique.AllowUserToAddRows = false;
            this.dataGridView_rubrique.AllowUserToDeleteRows = false;
            this.dataGridView_rubrique.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(239)))), ((int)(((byte)(249)))));
            this.dataGridView_rubrique.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_rubrique.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_rubrique.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView_rubrique.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(25)))), ((int)(((byte)(72)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_rubrique.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.DarkTurquoise;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_rubrique.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_rubrique.EnableHeadersVisualStyles = false;
            this.dataGridView_rubrique.Location = new System.Drawing.Point(80, 270);
            this.dataGridView_rubrique.MultiSelect = false;
            this.dataGridView_rubrique.Name = "dataGridView_rubrique";
            this.dataGridView_rubrique.ReadOnly = true;
            this.dataGridView_rubrique.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_rubrique.Size = new System.Drawing.Size(1440, 573);
            this.dataGridView_rubrique.TabIndex = 11;
            this.dataGridView_rubrique.TabStop = false;
            this.dataGridView_rubrique.Visible = false;
            this.dataGridView_rubrique.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_rubrique_CellContentDoubleClick);
            // 
            // textBox_searchCategory
            // 
            this.textBox_searchCategory.Location = new System.Drawing.Point(1130, 84);
            this.textBox_searchCategory.Name = "textBox_searchCategory";
            this.textBox_searchCategory.Size = new System.Drawing.Size(390, 20);
            this.textBox_searchCategory.TabIndex = 8;
            this.textBox_searchCategory.Visible = false;
            this.textBox_searchCategory.TextChanged += new System.EventHandler(this.textBox_searchCategory_TextChanged);
            // 
            // label_category
            // 
            this.label_category.AutoSize = true;
            this.label_category.Location = new System.Drawing.Point(955, 88);
            this.label_category.Name = "label_category";
            this.label_category.Size = new System.Drawing.Size(131, 13);
            this.label_category.TabIndex = 12;
            this.label_category.Text = "Recherche par catégorie :";
            this.label_category.Visible = false;
            // 
            // label_des
            // 
            this.label_des.AutoSize = true;
            this.label_des.Location = new System.Drawing.Point(948, 131);
            this.label_des.Name = "label_des";
            this.label_des.Size = new System.Drawing.Size(138, 13);
            this.label_des.TabIndex = 14;
            this.label_des.Text = "Recherche par description :";
            this.label_des.Visible = false;
            // 
            // textBox_searchDes
            // 
            this.textBox_searchDes.Location = new System.Drawing.Point(1130, 127);
            this.textBox_searchDes.Name = "textBox_searchDes";
            this.textBox_searchDes.Size = new System.Drawing.Size(390, 20);
            this.textBox_searchDes.TabIndex = 9;
            this.textBox_searchDes.Visible = false;
            this.textBox_searchDes.TextChanged += new System.EventHandler(this.textBox_searchDes_TextChanged);
            // 
            // label_lab
            // 
            this.label_lab.AutoSize = true;
            this.label_lab.Location = new System.Drawing.Point(977, 175);
            this.label_lab.Name = "label_lab";
            this.label_lab.Size = new System.Drawing.Size(109, 13);
            this.label_lab.TabIndex = 16;
            this.label_lab.Text = "Recherche par label :";
            this.label_lab.Visible = false;
            // 
            // textBox_searchLabel
            // 
            this.textBox_searchLabel.Location = new System.Drawing.Point(1130, 172);
            this.textBox_searchLabel.Name = "textBox_searchLabel";
            this.textBox_searchLabel.Size = new System.Drawing.Size(390, 20);
            this.textBox_searchLabel.TabIndex = 10;
            this.textBox_searchLabel.Visible = false;
            this.textBox_searchLabel.TextChanged += new System.EventHandler(this.textBox_searchLabel_TextChanged);
            // 
            // label_type
            // 
            this.label_type.AutoSize = true;
            this.label_type.Location = new System.Drawing.Point(926, 43);
            this.label_type.Name = "label_type";
            this.label_type.Size = new System.Drawing.Size(160, 13);
            this.label_type.TabIndex = 18;
            this.label_type.Text = "Recherche par numéro de type :";
            this.label_type.Visible = false;
            // 
            // textBox_searchType
            // 
            this.textBox_searchType.Location = new System.Drawing.Point(1130, 43);
            this.textBox_searchType.Name = "textBox_searchType";
            this.textBox_searchType.Size = new System.Drawing.Size(390, 20);
            this.textBox_searchType.TabIndex = 7;
            this.textBox_searchType.Visible = false;
            this.textBox_searchType.TextChanged += new System.EventHandler(this.textBox_searchType_TextChanged);
            // 
            // button_clear
            // 
            this.button_clear.Location = new System.Drawing.Point(1427, 215);
            this.button_clear.Name = "button_clear";
            this.button_clear.Size = new System.Drawing.Size(93, 23);
            this.button_clear.TabIndex = 20;
            this.button_clear.Text = "Reset";
            this.button_clear.UseVisualStyleBackColor = true;
            this.button_clear.Visible = false;
            this.button_clear.Click += new System.EventHandler(this.button_clear_Click);
            // 
            // button_critères
            // 
            this.button_critères.Location = new System.Drawing.Point(1265, 215);
            this.button_critères.Name = "button_critères";
            this.button_critères.Size = new System.Drawing.Size(121, 23);
            this.button_critères.TabIndex = 19;
            this.button_critères.Text = "Regroupement";
            this.button_critères.UseVisualStyleBackColor = true;
            this.button_critères.Visible = false;
            this.button_critères.Click += new System.EventHandler(this.button_communs_Click);
            // 
            // Recup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1600, 900);
            this.Controls.Add(this.button_critères);
            this.Controls.Add(this.button_clear);
            this.Controls.Add(this.label_type);
            this.Controls.Add(this.textBox_searchType);
            this.Controls.Add(this.label_lab);
            this.Controls.Add(this.textBox_searchLabel);
            this.Controls.Add(this.label_des);
            this.Controls.Add(this.textBox_searchDes);
            this.Controls.Add(this.label_category);
            this.Controls.Add(this.textBox_searchCategory);
            this.Controls.Add(this.dataGridView_rubrique);
            this.Controls.Add(this.textBox_chemin_fichier);
            this.Controls.Add(this.label_chemin_fichier);
            this.Controls.Add(this.button_parse);
            this.Controls.Add(this.button_fichier);
            this.Controls.Add(this.button_extract);
            this.Controls.Add(this.label_chemin_archive);
            this.Controls.Add(this.button_upload);
            this.Controls.Add(this.textBox_chemin_archive);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Recup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Récupération des cumuls";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_rubrique)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBox_chemin_archive;
        private System.Windows.Forms.Button button_upload;
        private System.Windows.Forms.Label label_chemin_archive;
        private System.Windows.Forms.OpenFileDialog openFileDialog_uparchive;
        private System.Windows.Forms.Button button_extract;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog_upextract;
        private System.Windows.Forms.Button button_fichier;
        private System.Windows.Forms.OpenFileDialog openFileDialog_xml;
        private System.Windows.Forms.Button button_parse;
        private System.Windows.Forms.Label label_chemin_fichier;
        private System.Windows.Forms.TextBox textBox_chemin_fichier;
        private System.Windows.Forms.DataGridView dataGridView_rubrique;
        private System.Windows.Forms.TextBox textBox_searchCategory;
        private System.Windows.Forms.Label label_category;
        private System.Windows.Forms.Label label_des;
        private System.Windows.Forms.TextBox textBox_searchDes;
        private System.Windows.Forms.Label label_lab;
        private System.Windows.Forms.TextBox textBox_searchLabel;
        private System.Windows.Forms.Label label_type;
        private System.Windows.Forms.TextBox textBox_searchType;
        private Button button_clear;
        private Button button_critères;
    }
}

