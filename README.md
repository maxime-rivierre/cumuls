# Installation du projet

## Installation de l'environnement

  - Télecharger Visual Studio ou tout autre IDE pouvant gérer les Windows Forms
    
    ## Récupération des sources
    
    - Télécharger l'archive .zip du projet
    - Ouvrir le fichier Cumuls.sln avec l'IDE 
    - Toutes les sources sont disponibles dans le dossier Cumuls
    
    # Accès au projet
    - Le dossier Demo dans Cumuls contient un éxécutable du logiciel
    - Le fichier XML est un exemple de fichier utilisable par le logiciel