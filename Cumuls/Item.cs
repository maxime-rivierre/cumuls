﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cumuls
{
    public class Item
    {
        private List<String> liste = new List<string>();

        public Item(List<String> p_liste)
        {
            liste = p_liste;                                                            // Item crée a partir d'une liste de paramètre, utilisé pour le regroupement
        }

        public List<String> GetCodes()
        {
            return liste;
        }

        public int NombreCodes()
        {
            int i = liste.Count;                                                        // Retourne le nombre d'élements dans l'Item
            return i;
        }
    }
}
