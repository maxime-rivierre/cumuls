﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cumuls
{
    public class Cumul
    {
        private String codeFillingMethod;                   //
        private String cumulType;                           //
        private String fillingMethod;                       //
        private String formula;                             // Valeurs pour cumuls
        private String sign;                                //
        private String uCodeCumulExportLabel;               //  

        public Cumul(String p_codeFillingMethod, String p_cumulType, String p_fillingMethod, String p_formula, String p_sign, String p_uCodeCumulExportlabel)
        {
            codeFillingMethod = p_codeFillingMethod;
            cumulType = p_cumulType;
            fillingMethod = p_fillingMethod;
            formula = p_formula;
            sign = p_sign;
            uCodeCumulExportLabel = p_uCodeCumulExportlabel;
        }
        public string CodeFillingMethod()
        {
            return codeFillingMethod;
        }

        public string CumulType()
        {
            return cumulType;
        }

        public string FillingMethod()
        {
            return fillingMethod;
        }

        public string Formula()
        {
            return formula;
        }

        public string Sign()
        {
            return sign;
        }

        public string CodeCumulLabel()
        {
            return uCodeCumulExportLabel;
        }
    }
}
