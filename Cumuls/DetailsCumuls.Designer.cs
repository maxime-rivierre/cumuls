﻿using System.Drawing;
using System.Windows.Forms;

namespace Cumuls
{
    partial class DetailsCumuls
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView_cumuls = new System.Windows.Forms.DataGridView();
            this.dataGridView_periodes = new System.Windows.Forms.DataGridView();
            this.label_cumuls = new System.Windows.Forms.Label();
            this.label_type = new System.Windows.Forms.Label();
            this.label_soustype = new System.Windows.Forms.Label();
            this.label_type_value = new System.Windows.Forms.Label();
            this.label_soustype_value = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_cumuls)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_periodes)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_cumuls
            // 
            this.dataGridView_cumuls.AllowUserToAddRows = false;
            this.dataGridView_cumuls.AllowUserToDeleteRows = false;
            this.dataGridView_cumuls.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(239)))), ((int)(((byte)(249)))));
            this.dataGridView_cumuls.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_cumuls.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_cumuls.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView_cumuls.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(25)))), ((int)(((byte)(72)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_cumuls.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.DarkTurquoise;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_cumuls.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_cumuls.EnableHeadersVisualStyles = false;
            this.dataGridView_cumuls.Location = new System.Drawing.Point(70, 38);
            this.dataGridView_cumuls.Name = "dataGridView_cumuls";
            this.dataGridView_cumuls.ReadOnly = true;
            this.dataGridView_cumuls.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_cumuls.Size = new System.Drawing.Size(810, 355);
            this.dataGridView_cumuls.TabIndex = 14;
            this.dataGridView_cumuls.Visible = false;
            // 
            // dataGridView_periodes
            // 
            this.dataGridView_periodes.AllowUserToAddRows = false;
            this.dataGridView_periodes.AllowUserToDeleteRows = false;
            this.dataGridView_periodes.AllowUserToOrderColumns = true;
            this.dataGridView_periodes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_periodes.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView_periodes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView_periodes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_periodes.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_periodes.EnableHeadersVisualStyles = false;
            this.dataGridView_periodes.Location = new System.Drawing.Point(70, 427);
            this.dataGridView_periodes.Name = "dataGridView_periodes";
            this.dataGridView_periodes.ReadOnly = true;
            this.dataGridView_periodes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_periodes.Size = new System.Drawing.Size(810, 47);
            this.dataGridView_periodes.TabIndex = 15;
            this.dataGridView_periodes.Visible = false;
            // 
            // label_cumuls
            // 
            this.label_cumuls.AutoSize = true;
            this.label_cumuls.Location = new System.Drawing.Point(366, 76);
            this.label_cumuls.Name = "label_cumuls";
            this.label_cumuls.Size = new System.Drawing.Size(35, 13);
            this.label_cumuls.TabIndex = 16;
            this.label_cumuls.Text = "label1";
            this.label_cumuls.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_cumuls.Visible = false;
            // 
            // label_type
            // 
            this.label_type.AutoSize = true;
            this.label_type.Location = new System.Drawing.Point(159, 492);
            this.label_type.Name = "label_type";
            this.label_type.Size = new System.Drawing.Size(40, 13);
            this.label_type.TabIndex = 17;
            this.label_type.Text = "Type : ";
            // 
            // label_soustype
            // 
            this.label_soustype.AutoSize = true;
            this.label_soustype.Location = new System.Drawing.Point(624, 492);
            this.label_soustype.Name = "label_soustype";
            this.label_soustype.Size = new System.Drawing.Size(60, 13);
            this.label_soustype.TabIndex = 18;
            this.label_soustype.Text = "Sous-type :";
            // 
            // label_type_value
            // 
            this.label_type_value.AutoSize = true;
            this.label_type_value.Location = new System.Drawing.Point(205, 492);
            this.label_type_value.Name = "label_type_value";
            this.label_type_value.Size = new System.Drawing.Size(35, 13);
            this.label_type_value.TabIndex = 19;
            this.label_type_value.Text = "label1";
            // 
            // label_soustype_value
            // 
            this.label_soustype_value.AutoSize = true;
            this.label_soustype_value.Location = new System.Drawing.Point(699, 492);
            this.label_soustype_value.Name = "label_soustype_value";
            this.label_soustype_value.Size = new System.Drawing.Size(35, 13);
            this.label_soustype_value.TabIndex = 20;
            this.label_soustype_value.Text = "label1";
            // 
            // DetailsCumuls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(966, 514);
            this.Controls.Add(this.label_soustype_value);
            this.Controls.Add(this.label_type_value);
            this.Controls.Add(this.label_soustype);
            this.Controls.Add(this.label_type);
            this.Controls.Add(this.label_cumuls);
            this.Controls.Add(this.dataGridView_periodes);
            this.Controls.Add(this.dataGridView_cumuls);
            this.Name = "DetailsCumuls";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Details";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_cumuls)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_periodes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView_cumuls;
        private System.Windows.Forms.DataGridView dataGridView_periodes;
        private Label label_cumuls;
        private Label label_type;
        private Label label_soustype;
        private Label label_type_value;
        private Label label_soustype_value;
    }
}